Hooks.on('createItem', (document, options, userId) => {
  if (CONFIG.system.testMode) console.debug("createItem:\n", [document, options, userId]);

  // items dragged from the sidebar will lose their moddedformula if they have one so calculate it again
  switch (document.type) {
    case "Melee-Attack":
    case "Ranged-Attack":
    case "Defence":
    case "Rollable": {
      let docdata = document.system;
      if (CONFIG.system.testMode) console.debug("Item Data:\n", docdata);
      if (docdata.formula) { // we will recalculate the value if the formula does not contain an @dependency
        let formula = docdata.formula;
        if (Number.isNumeric(formula)) {
          // the formula is a number
          docdata.value = Number(formula);
        } else if (formula.includes("#") || formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          docdata.value = 0;
          docdata.moddedvalue = 0;
          docdata.moddedformula = formula;
        }
      }
      break;
    }
    case "Modifier": {
      let docdata = document.system;
      if (docdata.alwaysOn) docdata.inEffect = true;
      break;
    }
  }
});

// TODO: clean this up because ruleset conflicts are not possible
Hooks.on('preCreateItem', (document, data, options, userId) => {
  if (CONFIG.system.testMode) console.debug("preCreateItem:\n", [document, data, options, userId]);

  let itemdata = data.system;
  if (itemdata == undefined) {
    // the item is being created from the item sidebar
    itemdata = document.system;
  }

  if (!itemdata.group) { // initialise group field to match item category or item type
    itemdata.group = itemdata.category || data.type;
  }

  if (!data.img) {
    data.img = "icons/svg/mystery-man-black.svg";
  }
  document.updateSource(data);
});
