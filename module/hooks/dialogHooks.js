/**
 * add listeners to dialog boxes
 */
Hooks.on("renderDialog", (dialog, html, data) => {
  // isolate these listeners to mydialog windows
  if (data.content?.includes("mydialog")) {
    const actor = dialog.data.options.actor;
    const macroData = dialog.data.options.macroData;

    // Update the GMod
    html.on("change", ".iteminput", async (ev) => {
      await actor.sheet._onInputChange(ev);
      await new Promise((r) => setTimeout(r, 200));
      macroData.position.left = ev.delegateTarget.offsetLeft;
      macroData.position.top = ev.delegateTarget.offsetTop - 3;
      actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      dialog.close();
    });

    // Select the attack to process
    html.on("click", ".selectable", (ev) => {
      macroData.position.left = ev.delegateTarget.offsetLeft;
      macroData.position.top = ev.delegateTarget.offsetTop - 3;
      actor.modifierdialog(ev.currentTarget.dataset.itemId, macroData);
      dialog.close();
    });

    // process the rollable item
    html.on("click", ".rollable", async (ev) => {
      actor.sheet._onRoll(ev);
      if (ev.currentTarget.dataset.type != "modlist") {
        await new Promise((r) => setTimeout(r, 200));
        macroData.position.left = ev.delegateTarget.offsetLeft;
        macroData.position.top = ev.delegateTarget.offsetTop - 3;
        actor.modifierdialog(ev.currentTarget.dataset.itemId, macroData);
        dialog.close();
      }
    });

    // allow modifiers to be toggled
    html.on("click", ".item-toggle", async (ev) => {
      await actor.sheet._onToggleItem(ev);
      macroData.position.left = ev.delegateTarget.offsetLeft;
      macroData.position.top = ev.delegateTarget.offsetTop - 3;
      actor.modifierdialog(ev.currentTarget.dataset.attackId, macroData);
      dialog.close();
    });
  }
});
