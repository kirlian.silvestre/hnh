/**
 * Perform a system migration for the entire World, applying migrations for Actors, Items, and Compendium packs
 * @return {Promise}      A Promise which resolves once the migration is completed
 */
export const migrateWorld = async function () {
  ui.notifications.notify(`Beginning Migration to HnH ${game.system.version}`, { permanent: true });

  // Migrate World Actors
  for (let a of game.actors.contents) {
    try {
      const updatedata = await migrateActorData(a);
      if (!foundry.utils.isEmpty(updatedata)) {
        console.debug(`Migrating Actor: ${a.name}`);
        await a.update(updatedata, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Actor ${a.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Items
  for (let i of game.items.contents) {
    try {
      const updatedata = await migrateItemData(i);
      if (!foundry.utils.isEmpty(updatedata)) {
        console.debug(`Migrating Item: ${i.name}`);
        await i.update(updatedata, { enforceTypes: false });
      }
    } catch (err) {
      err.message = `Failed migration for Item ${i.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate Actor Override Tokens
  for (let s of game.scenes.contents) {
    try {
      const updatedata = await migrateSceneData(s);
      if (!foundry.utils.isEmpty(updatedata)) {
        console.debug(`Migrating Scene: ${s.name}`);
        await s.update(updatedata, { enforceTypes: false });
        // If we do not do this, then synthetic token actors remain in cache
        // with the un-updated actordata.
        s.tokens.contents.forEach(t => t._actor = null);
      }
    } catch (err) {
      err.message = `Failed migration for Scene ${s.name}: ${err.message}`;
      console.error(err);
    }
  }

  // Migrate World Compendium Packs
  for (let p of game.packs) {
    if (p.metadata.packageType !== "world") continue; // comment out to migrate system packs
    if (!["Actor", "Item", "Scene"].includes(p.documentName)) continue;
    await migrateCompendium(p);
  }

  game.settings.set("hnh", "systemMigrationVersion", game.system.version);
  ui.notifications.notify(`Migration to HnH ${game.system.version} Finished`, { permanent: true });
}

/**
 * Apply migration rules to all Documents within a single Compendium pack
 * @param pack
 * @return {Promise}
 */
export const migrateCompendium = async function (pack) {
  const document = pack.documentName;
  if (!["Actor", "Item", "Scene"].includes(document)) return;

  // Unlock the pack for editing
  const wasLocked = pack.locked;
  await pack.configure({ locked: false });

  // Begin by requesting server-side data model migration and get the migrated content
  await pack.migrate();
  const documents = await pack.getDocuments();

  // Iterate over compendium entries - applying fine-tuned migration functions
  for (let doc of documents) {
    let updatedata = {};
    try {
      switch (document) {
        case "Actor":
          updatedata = await migrateActorData(doc);
          break;
        case "Item":
          updatedata = await migrateItemData(doc);
          break;
        case "Scene":
          updatedata = await migrateSceneData(doc);
          break;
      }

      // Save the entry, if data was changed
      if (foundry.utils.isEmpty(updatedata)) continue;
      await doc.update(updatedata);
      console.debug(`Migrated ${document} document ${doc.name} in Compendium ${pack.collection}`);
    }
    catch (err) {    // Handle migration failures
      err.message = `Failed migration for document ${doc.name} in pack ${pack.collection}: ${err.message}`;
      console.error(err);
    }
  }

  // Apply the original locked status for the pack
  await pack.configure({ locked: wasLocked });
  console.debug(`Migrated all ${document} documents from Compendium ${pack.collection}`);
};

/**
 * Migrate a single Actor document to incorporate latest data model changes
 * Return an Object of updatedata to be applied
 * @param {object} actor    The actor data object to update
 * @return {Object}         The updatedata to apply
 */
export const migrateActorData = function (actor) {
  const updatedata = { system: {} };

  // Actor Data Updates go here
  if (actor.system) {
    const todelete = [];
    let tobedeleted = false;
    // convert header character info items to system data
    updatedata.system.charinfo = {};
    for (let item of actor.system.items) {
      tobedeleted = false;
      switch (item?.name) {
        case "Temporary Health Points":
          tobedeleted = true;
          break;
        case "Race":
          updatedata.system.charinfo.race = item.system.notes;
          tobedeleted = true;
          break;
        case "Character Class":
          updatedata.system.charinfo.job = item.system.notes;
          tobedeleted = true;
          break;
        case "Specific Information":
          updatedata.system.charinfo.role = item.system.notes;
          tobedeleted = true;
          break;
        case "Additional Information":
          updatedata.system.charinfo.description = item.system.notes;
          tobedeleted = true;
          break;
        case "MPA":
          tobedeleted = true;
          break;
        case "Movement":
          if (item.type == "Modifier")
            tobedeleted = true;
          break;
      }
      if (item.type == "Rollable" && item.system.category == "spell") {
        tobedeleted = true;
      }
      if ("Hit-LocationPointsPowerIngredientAdvantage".includes(item.type))
        tobedeleted = true;
      if (tobedeleted) {
        todelete.push(item._id);
        console.log(`${item.name} to be deleted.`);
      }
    }
    actor.deleteEmbeddedDocuments("Item", todelete);

    // add a TempHP item
    const source = {
      name: "Temporary Health Points",
      type: "Pool",
      system: {
        group: "Health",
        abbr: "TempHP",
        notes: "To store temporary health."
      }
    };
    actor.createEmbeddedDocuments("Item", [source]);
  }

  // Migrate Owned Items
  if (!actor.items) return updatedata;
  const items = actor.items.reduce((arr, itemdata) => {
    // Migrate the Owned Item
    let itemUpdate = migrateItemData(itemdata);

    // Update the Owned Item
    if (!foundry.utils.isEmpty(itemUpdate)) {
      itemUpdate._id = itemdata._id;
      arr.push(expandObject(itemUpdate));
    }

    return arr;
  }, []);
  if (items.length > 0) updatedata.items = items;
  return updatedata;
}

/**
 * Migrate a single Item document to incorporate latest data model changes
 *
 * @param {object} item  Item data to migrate
 * @return {object}      The updatedata to apply
 */
export const migrateItemData = function (item) {
  let updatedata;
  switch (item.type) {
    case "Pool": {
      updatedata = _migratePools(item, updatedata);
      break;
    }
    case "Variable": {
      updatedata = _migrateVariables(item, updatedata);
      break;
    }
    case "Modifier": {
      updatedata = _migrateModifiers(item, updatedata);
      break;
    }
    case "Trait": {
      updatedata = _migrateTraits(item, updatedata);
      break;
    }
    case "Ranged-Attack":
    case "Melee-Attack":
    case "Rollable":
    case "Defence": {
      updatedata = _migrateRollables(item, updatedata);
      break;
    }
    default: {
      break;
    }
  }
  if (!updatedata) {
    updatedata = { system: {} };
  }
  try {
    updatedata.system.notes = jQuery(item.system.notes).text();
  } catch (err) {
    try {
      updatedata.system.notes = item.system.notes?.replace(/<[^>]*>?/gm, '') || "";
    } catch (err) {
      console.warn(`${item.name} notes migration might have failed.`);
    }
  }
  return updatedata;
}

/**
 * Migrate a single Scene document to incorporate changes to the data model of its actor data overrides
 * Return an Object of updatedata to be applied
 * @param {Object} scene  The Scene data to Update
 * @return {Object}       The updatedata to apply
 */
export const migrateSceneData = function (scene) {
  const tokens = scene.tokens.map(token => {
    const t = token.toJSON();
    if (!t.actorId || t.actorLink) {
      t.actordata = {};
    }
    else if (!game.actors.has(t.actorId)) {
      t.actorId = null;
      t.actordata = {};
    }
    else if (!t.actorLink) {
      try {
        const actordata = duplicate(t.actordata);
        actordata.type = token.actor?.type;
        const update = migrateActorData(actordata);
        ['items', 'effects'].forEach(embeddedName => {
          if (!update[embeddedName]?.length) return;
          const updates = new Map(update[embeddedName].map(u => [u._id, u]));
          t.actordata[embeddedName].forEach(original => {
            const update = updates.get(original._id);
            if (update) mergeObject(original, update);
          });
          delete update[embeddedName];
        });
        mergeObject(t.actordata, update);
      } catch (err) {
        console.warn(`${t.name} is broken on scene ${scene.name}.`);
      }
    }
    return t;
  });
  return { tokens };
};

/**
 * Convert some pools to Attributes and others to Variables
 * Convert hyphens to underscores in formulae
 *
 * @param {object} item        Item data to migrate
 * @param {object} updatedata  Existing update to expand upon
 * @return {object}            The updatedata to apply
 * @private
 */
function _migratePools(item, updatedata = {}) {
  updatedata.system = {
    minForm: `${item.system.minForm}`.replace(/([^\) -]+)-/g, "$1_"),
    maxForm: `${item.system.maxForm}`.replace(/([^\) -]+)-/g, "$1_")
  }
  return updatedata;
}

/**
 * Convert hyphens to underscores in formulae
 *
 * @param {object} item        Item data to migrate
 * @param {object} updatedata  Existing update to expand upon
 * @return {object}            The updatedata to apply
 * @private
 */
function _migrateRollables(item, updatedata = {}) {
  updatedata.system = {
    formula: `${item.system.formula}`.replace(/([^\) -]+)-/g, "$1_")
  }
  // convert notes to chat to traits
  if (item.system.category == "technique") {
    updatedata.type = "Trait";
    updatedata.system.category = "disadvantage";
    updatedata.system.group = "";
  }
  return updatedata;
}

/**
 * Convert hyphens to underscores in formulae
 *
 * @param {object} item        Item data to migrate
 * @param {object} updatedata  Existing update to expand upon
 * @return {object}            The updatedata to apply
 * @private
 */
function _migrateTraits(item, updatedata = {}) {
  // convert notes to chat to traits
  if (item.system.category == "advantage") {
    updatedata.system = { category: "disadvantage" };
  }
  return updatedata;
}

/**
 * Convert hyphens to underscores in formulae
 *
 * @param {object} item        Item data to migrate
 * @param {object} updatedata  Existing update to expand upon
 * @return {object}            The updatedata to apply
 * @private
 */
function _migrateModifiers(item, updatedata = {}) {
  let entries = Object.values(item.system.entries);
  updatedata.system = {
    entries: [...entries]
  }
  for (let i = 1; i < entries.length; i++) {
    updatedata.system.entries[i].formula = `${entries[i].formula}`.replace(/([^\) -]+)-/g, "$1_");
  }
  return updatedata;
}

/**
 * Convert hyphens to underscores in formulae
 *
 * @param {object} item        Item data to migrate
 * @param {object} updatedata  Existing update to expand upon
 * @return {object}            The updatedata to apply
 * @private
 */
function _migrateVariables(item, updatedata = {}) {
  let entries = Object.values(item.system.entries);
  updatedata.system = {
    entries: [...entries]
  }
  for (let i = 1; i < entries.length; i++) {
    updatedata.system.entries[i].formula = `${entries[i].formula}`.replace(/([^\) -]+)-/g, "$1_");
  }
  updatedata.system.formula = `${item.system.formula}`.replace(/([^\) -]+)-/g, "$1_");
  if (item.name.startsWith("Ranks:")) {
    // this is a skill ranks
    if ("BallisticsBrainPowerCunningMoxieGritBrawn".includes(entries[2]?.label)) {
      updatedata.system.group = "Skills";
    } else if (entries[1].label == "Damage Resistance") {
      updatedata.system.group = "Race";
    } else if (entries[1].label == "Level") {
      updatedata.system.group = "Advancement";
    } else {
      updatedata.system.group = "Job";
    }
  }
  return updatedata;
}

/**
 * Ready hook loads tables, and override's foundry's document link functions to provide extension to pseudo entities
 */
Hooks.once("ready", function () {

  if (CONFIG.system.testMode) console.debug("Starting Ready");

  // Determine whether a system migration is required and feasible
  if (!game.user.isGM) return;
  const currentVersion = game.settings.get("hnh", "systemMigrationVersion");
  const NEEDS_MIGRATION_VERSION = 0.01;
  const COMPATIBLE_MIGRATION_VERSION = 0.01;
  const totalDocuments = game.actors.size + game.scenes.size + game.items.size;
  if (!currentVersion && totalDocuments === 0) return game.settings.set("hnh", "systemMigrationVersion", game.system.version);
  const needsMigration = !currentVersion || isNewerVersion(NEEDS_MIGRATION_VERSION, currentVersion);
  if (!needsMigration) return;

  // Perform the migration
  if (currentVersion && isNewerVersion(COMPATIBLE_MIGRATION_VERSION, currentVersion)) {
    const warning = `Your system data is from too old a Foundry version and cannot be reliably migrated to the latest version. The process will be attempted, but errors may occur.`;
    ui.notifications.error(warning, { permanent: true });
  }
  return migrateWorld();
});
