export const config = {}

config.dataRgx = /[^-(){}\d<>/=*+., DdflorceiMmaxthbsunvkxX]/g;

CONFIG.ChatMessage.template = "systems/hnh/templates/chat/chat-message.hbs";

CONFIG.postures = [
  "systems/hnh/icons/postures/standing.png",
  "systems/hnh/icons/postures/sitting.png",
  "systems/hnh/icons/postures/crouching.png",
  "systems/hnh/icons/postures/crawling.png",
  "systems/hnh/icons/postures/kneeling.png",
  "systems/hnh/icons/postures/lyingback.png",
  "systems/hnh/icons/postures/lyingprone.png",
  "systems/hnh/icons/postures/sittingchair.png"
];
config.postures = {
  "standing": "local.postures.standing",
  "crouching": "local.postures.crouching",
  "kneeling": "local.postures.kneeling",
  "crawling": "local.postures.crawling",
  "sitting": "local.postures.sitting",
  "pronef": "local.postures.pronef",
  "proneb": "local.postures.proneb"
};

CONFIG.sizemods = [
  "systems/hnh/icons/sizemods/smneg1.png",
  "systems/hnh/icons/sizemods/smneg2.png",
  "systems/hnh/icons/sizemods/smneg3.png",
  "systems/hnh/icons/sizemods/smneg4.png",
  "systems/hnh/icons/sizemods/smpos1.png",
  "systems/hnh/icons/sizemods/smpos2.png",
  "systems/hnh/icons/sizemods/smpos3.png",
  "systems/hnh/icons/sizemods/smpos4.png"
];

CONFIG.crippled = [
  "systems/hnh/icons/crippled/crippledleftarm.png",
  "systems/hnh/icons/crippled/crippledlefthand.png",
  "systems/hnh/icons/crippled/crippledleftleg.png",
  "systems/hnh/icons/crippled/crippledleftfoot.png",
  "systems/hnh/icons/crippled/crippledrightarm.png",
  "systems/hnh/icons/crippled/crippledrighthand.png",
  "systems/hnh/icons/crippled/crippledrightleg.png",
  "systems/hnh/icons/crippled/crippledrightfoot.png",
];

CONFIG.statusEffects = [
  { icon: 'systems/hnh/icons/postures/standing.png', id: 'standing', label: 'local.postures.standing' },
  { icon: 'systems/hnh/icons/postures/sitting.png', id: 'sitting', label: 'local.postures.sitting' },
  { icon: 'systems/hnh/icons/postures/crouching.png', id: 'crouching', label: 'local.postures.crouching' },
  { icon: 'systems/hnh/icons/postures/crawling.png', id: 'crawling', label: 'local.postures.crawling' },
  { icon: 'systems/hnh/icons/postures/kneeling.png', id: 'kneeling', label: 'local.postures.kneeling' },
  { icon: 'systems/hnh/icons/postures/lyingback.png', id: 'lyingback', label: 'local.postures.proneb' },
  { icon: 'systems/hnh/icons/postures/lyingprone.png', id: 'lyingprone', label: 'local.postures.pronef' },
  { icon: 'systems/hnh/icons/postures/sittingchair.png', id: 'sittingchair', label: 'local.postures.sitting' },
  { icon: 'systems/hnh/icons/conditions/shock1.png', id: 'shock1', label: 'local.conditions.shock' },
  { icon: 'systems/hnh/icons/conditions/shock2.png', id: 'shock2', label: 'local.conditions.shock' },
  { icon: 'systems/hnh/icons/conditions/shock3.png', id: 'shock3', label: 'local.conditions.shock' },
  { icon: 'systems/hnh/icons/conditions/shock4.png', id: 'shock4', label: 'local.conditions.shock' },
  { icon: 'systems/hnh/icons/conditions/reeling.png', id: 'reeling', label: 'local.conditions.reeling' },
  { icon: 'systems/hnh/icons/conditions/tired.png', id: 'tired', label: 'local.conditions.tired' },
  { icon: 'systems/hnh/icons/conditions/collapse.png', id: 'collapse', label: 'local.conditions.collapse' },
  { icon: 'systems/hnh/icons/conditions/unconscious.png', id: 'unconscious', label: 'local.conditions.unconscious' },
  { icon: 'systems/hnh/icons/conditions/minus1xhp.png', id: 'minushp1', label: 'local.conditions.minushp' },
  { icon: 'systems/hnh/icons/conditions/minus2xhp.png', id: 'minushp2', label: 'local.conditions.minushp' },
  { icon: 'systems/hnh/icons/conditions/minus3xhp.png', id: 'minushp3', label: 'local.conditions.minushp' },
  { icon: 'systems/hnh/icons/conditions/minus4xhp.png', id: 'minushp4', label: 'local.conditions.minushp' },
  { icon: 'systems/hnh/icons/conditions/stunned.png', id: 'stunned', label: 'local.conditions.stunned' },
  { icon: 'systems/hnh/icons/conditions/surprised.png', id: 'surprised', label: 'local.conditions.surprised' },
  { icon: 'systems/hnh/icons/defeated.png', id: 'defeated', label: 'local.conditions.defeated' },
  { icon: 'systems/hnh/icons/blank.png', id: 'none', label: 'local.conditions.none' },
  { icon: 'systems/hnh/icons/stances/hth.svg', id: 'hth', label: 'local.stances.hth' },
  { icon: 'systems/hnh/icons/stances/magic.svg', id: 'magic', label: 'local.stances.magic' },
  { icon: 'systems/hnh/icons/stances/ranged.svg', id: 'ranged', label: 'local.stances.ranged' },
  { icon: 'systems/hnh/icons/stances/thrown.svg', id: 'thrown', label: 'local.stances.thrown' },
  { icon: 'systems/hnh/icons/crippled/crippledleftarm.png', id: 'crippledleftarm', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledlefthand.png', id: 'crippledlefthand', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledleftleg.png', id: 'crippledleftleg', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledleftfoot.png', id: 'crippledleftfoot', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledrightarm.png', id: 'crippledrightarm', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledrighthand.png', id: 'crippledrighthand', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledrightleg.png', id: 'crippledrightleg', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/crippled/crippledrightfoot.png', id: 'crippledrightfoot', label: 'local.conditions.crippled' },
  { icon: 'systems/hnh/icons/sizemods/smneg1.png', id: 'smaller1', label: 'local.conditions.smaller' },
  { icon: 'systems/hnh/icons/sizemods/smneg2.png', id: 'smaller2', label: 'local.conditions.smaller' },
  { icon: 'systems/hnh/icons/sizemods/smneg3.png', id: 'smaller3', label: 'local.conditions.smaller' },
  { icon: 'systems/hnh/icons/sizemods/smneg4.png', id: 'smaller4', label: 'local.conditions.smaller' },
  { icon: 'systems/hnh/icons/sizemods/smpos1.png', id: 'larger1', label: 'local.conditions.larger' },
  { icon: 'systems/hnh/icons/sizemods/smpos2.png', id: 'larger2', label: 'local.conditions.larger' },
  { icon: 'systems/hnh/icons/sizemods/smpos3.png', id: 'larger3', label: 'local.conditions.larger' },
  { icon: 'systems/hnh/icons/sizemods/smpos4.png', id: 'larger4', label: 'local.conditions.larger' }
];

CONFIG.controlIcons.defeated = "systems/hnh/icons/defeated.png";

CONFIG.tokenEffects = {
  effectSize: {
    xLarge: 2,
    large: 3,
    medium: 4,
    small: 5
  },
  effectSizeChoices: {
    small: "Small (Default) - 5x5",
    medium: "Medium - 4x4",
    large: "Large - 3x3",
    xLarge: "Extra Large - 2x2"
  }
};

config.combat = {
  defaultActionCost: 1,
  defaultShotCost: 3,
  missingInitiative: "You must roll initiative first."
};
config.variables = {
  attributes: ["str", "agi", "dex", "end", "int", "kno", "sen", "will", "pre", "emp", "com", "inf", "fate"],
  characteristics: ["bd", "wl", "tl", "speed", "md", "il", "fl", "size", "med", "mth", "ith", "wth"],
  skillvariables: ["withadvantage"],
  spellvariables: [],
  defencevariables: [],
  attackvariables: ["withadvantage", "range"],
};
