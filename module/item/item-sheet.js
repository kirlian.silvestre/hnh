/**
 * Extend the basic ItemSheet with some very simple modifications
 * @extends {ItemSheet}
 */
export class BaseItemSheet extends ItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hnh", "sheet", "item"],
      width: 450,
      height: 450,
    });
  }

  /** @override */
  get template() {
    const path = "systems/hnh/templates/item";
    // Return a single sheet for all item types.
    // return `${path}/item-sheet.hbs`;
    // Alternatively, you could use the following return statement to do a
    // unique item sheet by type, like `weapon-sheet.hbs`.

    return `${path}/${this.item.type.toLowerCase()}-sheet.hbs`;
  }

  /* -------------------------------------------- */

  /** @override */
  async getData(options) {
    const context = await super.getData(options);
    const item = context.item;
    context.system = item.system;
    context.config = CONFIG.system;
    //context.enrichment = {      system: {        notes: TextEditor.enrichHTML(this.item.system.notes, { async: false, secrets: this.item.isOwner, relativeTo: this.item })      }    };
    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    const handler = (ev) => this._onDragStart(ev);
    html.find(".draggable").each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, true);
    });
  }

  /**
   * Replace referenced data attributes in the formula with the syntax `@attr` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    if (this.actor) {
      const dataRgx = /[@]([\w.]+)/gi;
      const dynamic = this.actor.system.dynamic;
      const tracked = this.actor.system.tracked;
      const findTerms = (match, term) => {
        const fields = term.split(".");
        const attribute = fields[0];
        // default to the field "moddedvalue" if none is specified
        const field = fields[1] || "moddedvalue";
        if (dynamic[attribute]) {
          const value = dynamic[attribute].system[field];
          return (value) ? String(value).trim() : "0";
        } else if (tracked[attribute]) {
          const value = tracked[attribute].system[field];
          return (value) ? String(value).trim() : "0";
        } else {
          return "0";
        }
      };
      return { value: formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)/g, "Math.$&") };
    } else {
      return { value: "0" };
    }
  }
}
export class PoolItemSheet extends BaseItemSheet {

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.pool').change(this._onInputChange.bind(this));
  }

  /**
   * Handle the Item Input Change Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onInputChange(event) { // only for pools as they must update tokens
    if (CONFIG.system.testMode) console.debug("entering _onInputChange()\n", event);

    if (this.actor) {
      const itemID = this.actor.slugify(this.item.system.abbr);
      const target = event.currentTarget;
      // get the name of the changed element
      const dataname = target.name.split(".")[1];
      // get the new value
      const value = target.value;
      // is this the value attribute, isBar is true
      const isBar = (dataname == "value");

      // make the changes to tokens, actors and the item
      this.actor.modifyTokenAttribute(`tracked.${itemID}${isBar ? '' : `.${dataname}`}`, value, false, isBar);
    }
  }
}

export class ModifierItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hnh", "sheet", "item", "modifiersheet"],
      width: 600,
      height: 400,
      dragDrop: [
        { dragSelector: '.modentry-drag', dropSelector: '.modentry' },
      ],
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.modentry-create').click(this._onModEntryCreate.bind(this));
    html.find('.modentry-delete').click(this._onModEntryDelete.bind(this));
    html.find('.modentry-clone').click(this._onModEntryClone.bind(this));
  }

  async _onDragStart(event) {
    const dataset = event.currentTarget.dataset;

    // Create drag data
    let dragData = {
      itemId: dataset.itemId,
      index: dataset.index,
      type: "modentry",
      value: dataset.value,
      category: dataset.category,
      formula: dataset.formula,
      targets: dataset.targets
    }

    if (!dragData) return;

    // Set data transfer
    await event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  async _onDrop(event) {
    // Try to extract the data
    let transferData;
    try {
      transferData = JSON.parse(event.dataTransfer.getData("text/plain"));
    } catch (err) {
      return false;
    }
    if (CONFIG.system.testMode) console.debug(`processing ${transferData.type}\n`, transferData);

    const targetindex = Number(event.currentTarget.dataset.id);
    const sourceindex = Number(transferData.index);
    const sourceItemId = transferData.itemId;
    let formula;
    let targets;
    let value;
    let category;
    switch (transferData.type) {
      case "varentry": {
        value = transferData.value;
        formula = transferData.formula;
        category = "attack";
        targets = transferData.label;
        break;
      }
      case "modentry": {
        value = transferData.value;
        formula = transferData.formula;
        category = transferData.category;
        targets = transferData.targets;
        break;
      }
    }
    const entry = {
      value: value,
      formula: formula,
      category: category,
      targets: targets
    };
    const itemdata = this.item.system;
    const entries = Object.values(itemdata.entries);
    let newsource = sourceindex;

    if (this.item.id != sourceItemId) {
      // inserting an entry
      entries.push(entry);
      newsource = entries.length - 1;
    }
    // re-ordering entries
    if (newsource > targetindex) {
      // moving up
      for (let i = newsource - 1; i != targetindex; i--) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource--;
      }
    } else {
      // moving down
      for (let i = newsource + 1; i != targetindex; i++) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource++;
      }
    }

    let data = {
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: entries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Modifier Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onModEntryCreate()\n", event);
    let itemdata = this.item.system;

    // no formula, must be accident
    if (itemdata.entries[0].formula.trim() == "") return;

    let modentries = Object.values(itemdata.entries);
    let entry = modentries[0];
    let formData = this._replaceData(entry.formula);
    try {
      entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')));
      entry.moddedformula = "" + entry.value; // store the result as a String
    } catch (err) {
      // store the formula ready to be rolled
      entry.moddedformula = formData.value;
      entry.value = 0; // eliminate any old values
      console.debug("Modifier formula evaluation error:\n", [entry.moddedformula]);
    }
    modentries.push(entry);
    modentries[0] = { value: 0, formula: "", category: "", targets: "" };
    let data = {
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Modifier Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onModEntryDelete()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;

    let itemdata = this.item.system;
    delete itemdata.entries[index];
    let modentries = Object.values(itemdata.entries);

    let data = {
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Modifier Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onModEntryClone(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onModEntryClone()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".modentry").dataset.id;

    let itemdata = this.item.system;
    let modentries = Object.values(itemdata.entries);
    modentries.push({ ...modentries[index] });

    let data = {
      inEffect: itemdata.inEffect,
      notes: itemdata.notes,
      entries: modentries
    }
    this.item.update({ 'system': data });
  }
}

export class VariableItemSheet extends BaseItemSheet {

  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hnh", "sheet", "item", "variablesheet"],
      width: 300,
      height: 400,
      dragDrop: [
        { dragSelector: '.varentry-drag', dropSelector: '.varentry' },
      ],
    });
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    // Everything below here is only needed if the sheet is editable
    if (!this.options.editable) return;

    // Roll handlers, click handlers, etc. would go here.
    html.find('.varentry-create').click(this._onVarEntryCreate.bind(this));
    html.find('.varentry-delete').click(this._onVarEntryDelete.bind(this));
    html.find('.varentry-clone').click(this._onVarEntryClone.bind(this));

    // Roll handlers, click handlers, etc. would go here.
    const handler = (ev) => this._onDragStart(ev);
    html.find(".draggable").each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, true);
    });
  }

  async _onDragStart(event) {
    const dataset = event.currentTarget.dataset;

    // Create drag data
    let dragData = {
      itemId: dataset.itemId,
      index: dataset.index,
      type: "varentry",
      value: dataset.value,
      formula: dataset.formula,
      label: dataset.label
    }

    if (!dragData) return;

    // Set data transfer
    await event.dataTransfer.setData("text/plain", JSON.stringify(dragData));
  }

  async _onDrop(event) {
    // Try to extract the data
    let transferData;
    try {
      transferData = JSON.parse(event.dataTransfer.getData("text/plain"));
    } catch (err) {
      return false;
    }
    if (CONFIG.system.testMode) console.debug(`processing ${transferData.type}\n`, transferData);

    const targetindex = Number(event.currentTarget.dataset.id);
    const sourceindex = Number(transferData.index);
    const sourceItemId = transferData.itemId;
    let formula;
    let label;
    let value;
    switch (transferData.type) {
      case "varentry": {
        value = transferData.value;
        formula = transferData.formula;
        label = transferData.label;
        break;
      }
      case "modentry": {
        value = transferData.value;
        formula = transferData.formula;
        label = transferData.targets;
        break;
      }
    }
    const entry = {
      value: value,
      formula: formula,
      label: label
    };
    const itemdata = this.item.system;
    const entries = Object.values(itemdata.entries);
    let newsource = sourceindex;

    if (this.item.id != sourceItemId) {
      // inserting an entry
      entries.push(entry);
      newsource = entries.length - 1;
    }
    // re-ordering entries
    if (newsource > targetindex) {
      // moving up
      for (let i = newsource - 1; i != targetindex; i--) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource--;
      }
    } else {
      // moving down
      for (let i = newsource + 1; i != targetindex; i++) {
        [entries[i], entries[newsource]] = [entries[newsource], entries[i]];
        newsource++;
      }
    }

    let data = {
      notes: itemdata.notes,
      entries: entries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Variable Entry Create click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryCreate(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onVarEntryCreate()\n", event);
    let itemdata = this.item.system;

    // no formula, must be accident
    if (itemdata.entries[0].formula.trim() == "") return;

    let varentries = Object.values(itemdata.entries);
    let entry = varentries[0];
    let formData = this._replaceData(entry.formula);
    try {
      entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, '')));
      entry.moddedformula = "" + entry.value; // store the result as a String
    } catch (err) {
      // store the formula ready to be rolled
      entry.moddedformula = formData.value;
      entry.value = 0; // eliminate any old values
      console.debug("Modifier formula evaluation error:\n", [entry.moddedformula]);
    }
    varentries.push(entry);
    varentries[0] = { value: 0, formula: "", label: "" };
    let data = {
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Variable Entry Delete click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryDelete(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onVarEntryDelete()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;

    let itemdata = this.item.system;
    delete itemdata.entries[index]; // not an array at this point
    let varentries = Object.values(itemdata.entries);

    let data = {
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'system': data });
  }

  /**
   * Handle the Variable Entry Clone click Event
   * @param {Event} event   The originating click event
   * @private
   */
  _onVarEntryClone(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onVarEntryClone()\n", event);
    let element = event.currentTarget;
    let index = element.closest(".varentry").dataset.id;

    let itemdata = this.item.system;
    let varentries = Object.values(itemdata.entries);
    varentries.push({ ...varentries[index] });

    let data = {
      notes: itemdata.notes,
      entries: varentries
    }
    this.item.update({ 'system': data });
  }
}
