/**
 * Extend the basic Item with some very simple modifications.
 * @extends {Item}
 */
export class BaseItem extends Item {
  /**
   * Augment the basic Item data model with additional dynamic data.
   */
  prepareDerivedData() {
    const itemdata = this.system;
    switch (this.type) {
      case "Variable": {
        let entries = Object.values(itemdata.entries);
        for (let i = 1; i < entries.length; i++) {
          entries[i].value = Number(entries[i].formula) || 0; // recalculate the value
          if (itemdata.label == "") itemdata.label = entries[i].label;
          // if the entry label matches the variable label, write the formula and value
          if (entries[i].label == itemdata.label) {
            itemdata.moddedformula = itemdata.formula = entries[i].formula;
            itemdata.value = itemdata.moddedvalue = entries[i].value;
          }
        }
        break;
      }
      case "Modifier": {
        if (itemdata.alwaysOn) itemdata.inEffect = true;
        itemdata.attack = false;
        itemdata.damage = false;
        itemdata.defence = false;
        itemdata.skill = false;
        itemdata.attribute = false;
        let entries = Object.values(itemdata.entries);
        for (let i = 1; i < entries.length; i++) { //ignore category changes to [0]
          switch (entries[i].category) {
            case "attack": itemdata.attack = true; break;
            case "damage": itemdata.damage = true; break;
            case "defence": itemdata.defence = true; break;
            case "skill": itemdata.skill = true; break;
            case "attribute": itemdata.attribute = true; break;
          }
          if (Number.isNumeric(entries[i].formula)) {
            // the formula is a number
            entries[i].value = entries[i].moddedvalue = Number(entries[i].formula);
          } else if (entries[i].formula.includes("@")) {
            // the formula has a reference and will be processed in prepareDerivedData
          } else {
            // the formula should be a valid dice expression so has no value
            entries[i].value = 0;
            entries[i].moddedvalue = 0;
            entries[i].moddedformula = entries[i].formula;
          }
        }
        break;
      }
      case "Characteristic":
      case "Melee-Attack":
      case "Ranged-Attack":
      case "Defence":
      case "Rollable": {
        if (Number.isNumeric(itemdata.formula)) {
          // the formula is a number
          itemdata.value = itemdata.moddedvalue = Number(itemdata.formula);
        } else if (itemdata.formula.includes("#") || itemdata.formula.includes("@")) {
          // the formula has ranks or a reference and will be processed in prepareDerivedData
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.value = 0;
          itemdata.moddedvalue = 0;
          itemdata.moddedformula = itemdata.formula;
        }
        break;
      }
      case "Pool": {
        if (Number.isNumeric(itemdata.maxForm)) {
          // the formula is a number
          itemdata.max = Number(itemdata.maxForm);
          itemdata.hasmax = true;
        } else if (itemdata.maxForm.includes("@")) {
          // the formula has a reference and will be processed in prepareDerivedData
          itemdata.hasmax = false;
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.max = 0;
          itemdata.hasmax = true;
        }
        if (Number.isNumeric(itemdata.minForm)) {
          // the formula is a number
          itemdata.min = Number(itemdata.minForm);
          itemdata.hasmin = true;
        } else if (itemdata.minForm.includes("@")) {
          // the formula has a reference and will be processed in prepareDerivedData
          itemdata.hasmin = false;
        } else {
          // the formula should be a valid dice expression so has no value
          itemdata.min = 0;
          itemdata.hasmin = true;
        }
        if (itemdata.hasmax && itemdata.value > itemdata.max) itemdata.value = itemdata.max;
        if (itemdata.hasmin && itemdata.value < itemdata.min) itemdata.value = itemdata.min;
        itemdata.moddedvalue = itemdata.value;
        itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
        break;
      }
      default: {
        // do nothing yet
      }
    }
  }
}
