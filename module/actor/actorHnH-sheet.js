import { config } from "../config.js";
import { BaseActorSheet } from "./baseActor-sheet.js";

/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {BaseActorSheet}
 */
export class ActorHnHSheet extends BaseActorSheet {
  /** @override */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["hnh", "sheet", "actor"],
      template: "systems/hnh/templates/actor/actorHnH-sheet.hbs",
      width: 600,
      height: 800,
      tabs: [
        {
          navSelector: ".sheet-nav",
          contentSelector: ".sheet-body",
          initial: "combat",
        },
      ],
    });
  }

  /** @override */
  getData(options) {
    if (CONFIG.system.testMode)
      console.debug("entering getData() in actorHnH-sheet");

    const context = super.getData(options);
    const actor = context.actor;

    // My shortcuts
    const actordata = actor.system;
    const dynamic = actordata.dynamic;
    const tracked = actordata.tracked;
    const headinfo = actordata.headerinfo = {};

    headinfo.level = dynamic.level;
    headinfo.dr = dynamic.damage_resistance;
    headinfo.hitpoints = tracked.hp;
    headinfo.apr = dynamic.apr;
    headinfo.pace = dynamic.move_mode;

    for (let item of actordata.traits) {
      switch (item?.name) {
        case "Race":
          headinfo.race = item;
          break;
        case "Character Class":
          headinfo.characterclass = item;
          break;
        case "Specific Information":
          headinfo.specificinfo = item;
          break;
        case "Additional Information":
          headinfo.additionalinfo = item;
          break;
      }
    }

    // group and sort rankitems correctly for display
    const rid = {};
    for (const item of actordata.rankitems) {
      const group = item.system.group;
      item.system.displayname = item.name.split("nks:")[1].trim();
      if (!rid[group]) {
        rid[group] = {
          name: group,
          items: []
        };
      }
      rid[group].items.push(item);
    }
    actordata.rankitemsdisplay = this.sort(Object.values(rid));

    // group and sort skills correctly for display
    const tempskills = [...actordata.skills];
    const tempspells = [...actordata.spells];
    actordata.allskills = [];
    actordata.skills = [];
    actordata.techniques = [];
    for (const item of tempskills) {
      if (item.system.category == "technique") {
        actordata.techniques.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        actordata.skills.push(item);
        actordata.allskills.push(item);
      }
    }
    actordata.spells = [];
    actordata.rms = [];
    for (let item of tempspells) {
      if (item.system.category == "rms") {
        actordata.rms.push(item);
      } else {
        actordata.spells.push(item);
        actordata.allskills.push(item);
      }
    }
    for (let item of actordata.checks) {
      actordata.allskills.push(item);
    }
    actordata.allskills = this.sort(actordata.allskills);

    // group and sort defences correctly for display
    let tempdefences = [...actordata.defences];
    actordata.defences = [];
    actordata.blocks = [];
    actordata.parrys = [];
    actordata.dodges = [];
    for (let item of tempdefences) {
      if (item.system.category == "block") {
        actordata.blocks.push(item);
      } else {
        item.system.stepvalue = item.system.moddedvalue - item.system.value;
        actordata.defences.push(item);
        if (item.system.category == "dodge") {
          actordata.dodges.push(item);
        } else {
          actordata.parrys.push(item);
        }
      }
    }

    actordata.attributes = [];
    for (let varname of config.variables.attributes) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.attributes.push(dynamic[varname]);
    }
    actordata.characteristics = [];
    for (let varname of config.variables.characteristics) {
      if (!varname.trim()) continue;
      if (dynamic[varname]) actordata.characteristics.push(dynamic[varname]);
    }

    actordata.attackvariables = [];
    for (let varname of config.variables.attackvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        actordata.attackvariables.push(dynamic[varname]);
      else if (tracked[varname])
        actordata.attackvariables.push(tracked[varname]);
    }
    for (let pooldata of actordata.pools) {
      if (pooldata.name.startsWith("Ammo:")) actordata.attackvariables.push(pooldata);
    }
    actordata.defencevariables = [];
    for (let varname of config.variables.defencevariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        actordata.defencevariables.push(dynamic[varname]);
      else if (tracked[varname])
        actordata.defencevariables.push(tracked[varname]);
    }
    actordata.skillvariables = [];
    for (let varname of config.variables.skillvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        actordata.skillvariables.push(dynamic[varname]);
      else if (tracked[varname])
        actordata.skillvariables.push(tracked[varname]);
    }
    actordata.spellvariables = [];
    for (let varname of config.variables.spellvariables) {
      if (!varname.trim()) continue;
      if (dynamic[varname])
        actordata.spellvariables.push(dynamic[varname]);
      else if (tracked[varname])
        actordata.spellvariables.push(tracked[varname]);
    }

    // group and sort skill mods
    actordata.checkskillspellmods = this.sort(
      Array.from(new Set(actordata.checkmods.concat(actordata.skillmods, actordata.spellmods)))
    );

    delete actordata.skillmods;
    delete actordata.checkmods;
    delete actordata.spellmods;

    // check to see if specific modifiers are toggled so we can set the flags
    // the modifier must affect Variable Formulae, D20 skills or D100 skills to work as a toggle (a modifier of zero is ok)
    // TODO: move these to BaseActor if they are useful
    for (let item of actordata.checkskillspellmods) {
      switch (item?.name) {
        case "Rushed":
          actor.setFlag("hnh", "rushed", item.system.inEffect);
          break;
        case "Using Edge":
          actor.setFlag("hnh", "usingedge", item.system.inEffect);
          break;
      }
    }

    return context;
  }

  sort(actordata) {
    return actordata.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });
  }
}
