/**
 * Extend the base Actor entity by defining a custom roll data structure which is ideal for the Simple system.
 * @extends {Actor}
 */
export class BaseActor extends Actor {
  /**
   * Fetch and sort all the items, even though they will be prepared internally after this stage.
   */
  prepareBaseData() {
    const actordata = this.system;
    if (CONFIG.system.testMode)
      console.debug("entering prepareBaseData()\n", [this, actordata]);

    actordata.items = Array.from(this.items.values()).sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    /**
     * Reference data structure to mimic static systems
     */
    actordata.skills = [];
    actordata.defence = {};
    actordata.dynamic = {};
    actordata.modifiers = {};
    actordata.tracked = {};
    actordata.rankitems = [];

    // set some basic values
    actordata.dynamic.scale = { value: game.scenes?.current?.grid.distance || 5, };
  }

  /**
   * Augment the basic actor data with additional dynamic data.
   *
   * Changes made here to item seem to stick.
   */
  prepareDerivedData() {
    const actor = this;
    const actordata = actor.system;

    if (CONFIG.system.testMode)
      console.debug("entering prepareDerivedData()\n", [actor, actordata]);

    // for counting the ranks stored in Variable Items for some systems
    const actorranks = {};

    // group the items for efficient iteration
    const actormods = [];
    const attributemods = [];
    const actornonmods = [];
    const actorrollables = [];

    // store references to attribute attribute and pool items
    let previousLayer = [];
    let nextLayer = [];

    /**
     * FIXME: TODO: Move as much item calculation as possible to the item preparederiveddata method
     * 
     * There is a shift from item.value as the default reference to item.moddedvalue and a lot of the 
     * artificial references are no longer required because we are accessing the items directly all 
     * the time now.
     */

    // write the items into the model
    for (const item of actordata.items) {
      const itemdata = item.system;
      let itemID = actor.slugify(item.name);
      // collect non-modifier items with formula
      if (itemdata.formula) actornonmods.push(item);

      switch (item.type) {
        case "Attribute": {
          itemID = actor.slugify(itemdata.abbr);
          // only process attributes with abbreviations
          if (itemID) {
            actordata.dynamic[itemID] = item;
            nextLayer.push(itemID);
          }
          break;
        }
        case "Characteristic": {
          itemID = actor.slugify(itemdata.abbr);
          // only process attributes with abbreviations
          if (itemID) {
            actordata.dynamic[itemID] = item;
            if (Number.isNumeric(itemdata.formula))
              nextLayer.push(itemID);
          }
          break;
        }
        case "Skill": {
          actordata.dynamic[itemID] = item;
          if (Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#"))
            nextLayer.push(itemID);
          actordata.skills.push(item);
          break;
        }
        case "Pool": {
          itemID = actor.slugify(itemdata.abbr);
          if (itemID) {
            actordata.tracked[itemID] = item;
            if (itemdata.hasmax && itemdata.hasmin) {
              nextLayer.push(itemID);
            } else {
              actornonmods.push(item);
            }
          }
          break;
        }
        case "Modifier": {
          itemdata.tempName = item.name;
          let itemID = actor.slugify(item.name);
          actordata.modifiers[itemID] = item;
          actormods.push(item);
          if (itemdata.attribute) attributemods.push(item);
          break;
        }
        case "Variable": {
          // add any items whose name starts with "Ranks:" to the list to be polled next
          if (item.name.startsWith("Ranks:")) actordata.rankitems.push(item);
        }
        case "Defence":
        case "Rollable":
        case "Melee-Attack":
        case "Ranged-Attack": {
          actordata.dynamic[itemID] = item;
          if (Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#"))
            nextLayer.push(itemID);
          actorrollables.push(item);
          break;
        }
        default: {
          // Traits that define a vision type for the actor
          switch (item.name) {
            case "Keen Senses":
            case "Night Sight":
            case "Dark Sight":
            case "Darkvision":
            case "Star Sight": {
              actordata.dynamic.visionType = item.name;
            }
          }
        }
      }
    }

    // calculate the ranks, then process number formulae
    if (CONFIG.system.testMode)
      console.debug("Rankitems to be polled:\n", [this, actordata.rankitems]);

    // poll the rank items for systems which count them
    for (const item of actordata.rankitems) {
      for (const entry of Object.values(item.system.entries)) {
        const target = actor.slugify(entry.label);
        // if the name is not blank
        if (target) {
          entry.value = Number(entry.formula);
          // the target may be one that does not exist on the actor so capture it anyway
          actorranks[target] = actorranks[target] ? actorranks[target] + entry.value : entry.value;
        }
      }
    }

    if (CONFIG.system.testMode)
      console.debug("Rankdata after polling:\n", [this, actorranks]);

    // assign the ranks to the targets in dynamic
    for (const [name, ranks] of Object.entries(actorranks)) {
      const item = actordata.dynamic[name];
      if (item) {
        const itemdata = item.system;
        // calculate rank bonus to assign to value and moddedvalue
        if (item.type == "Attribute") {
          itemdata.attr = ranks;
        } else {
          // set the formula text field
          itemdata.formula = `#${ranks}`;
        }
        itemdata.moddedvalue = itemdata.value = itemdata.ranks = ranks;
      } else {
        if (CONFIG.system.testMode)
          console.debug(`${ranks} Ranks were not assigned to : ${name} which was undefined`);
      }
    }

    /**
     * Process Attributes
     */
    for (const item of attributemods) {
      const itemdata = item.system;
      if (itemdata.inEffect) {
        for (const entry of Object.values(itemdata.entries)) {
          // this entry is a attributemod
          if (entry.category == "attribute") {
            // process attributemods formulae before the mods are applied
            actor.processModifierFormula(entry, itemdata);
            const cases = entry.targets.split(",").map(word => word.trim().toLowerCase());
            for (const key of cases) {
              const target = actordata.dynamic[key];
              if (target?.type == "Attribute") {
                target.system.value = target.system.moddedvalue += entry.value;
              }
            }
          }
        }
      }
    }

    /**
     * Calculate every item with a formula, one layer at a time, starting with
     * those whose references have been calculated. Once an item has been
     * processed, add it to the layer of those available as references.
     *
     * TODO: This section needs another look to see if there is a way to
     * avoid the requirement to have all reference depths the same.
     * 
     * I think I have it now. The previousLayer needs to end up being all processed items
     */
    while (nextLayer.length !== 0) {
      previousLayer = previousLayer.concat(nextLayer);
      nextLayer = [];

      for (const item of actornonmods) {
        const itemdata = item.system;
        if (item.type == "Pool") {
          const dynID = actor.slugify(itemdata.abbr);

          //ignore numeric formulae that have been calculated by the item
          if (itemdata.hasmax && itemdata.hasmin) continue;

          if (!itemdata.hasmax) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.maxForm.includes(target)) {
                // write to Actor Item
                let formData = actor._replaceData(itemdata.maxForm);
                try {
                  itemdata.max = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.min(itemdata.value, itemdata.max);
                } catch (err) {
                  itemdata.max = 0; // eliminate any old values
                  console.error("Pool-max formula evaluation error:\n", [itemdata.maxForm,]);
                }
                itemdata.hasmax = true;
              }
            }
          }
          if (!itemdata.hasmin) {
            // formula has not yet been calculated
            for (const target of previousLayer) {
              // if this formula refers to an item in the previous layer then process it
              if (itemdata.minForm.includes(target)) {
                // write to Actor Item
                let formData = actor._replaceData(itemdata.minForm);
                try {
                  itemdata.min = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                  itemdata.moddedvalue = itemdata.value = Math.max(itemdata.value, itemdata.min);
                } catch (err) {
                  itemdata.min = 0; // eliminate any old values
                  console.error("Pool-min formula evaluation error:\n", [itemdata.minForm,]);
                }
                itemdata.hasmin = true;
              }
            }
          }
          itemdata.ratio = Math.trunc(itemdata.value / itemdata.max * 100);
          if (itemdata.hasmax && itemdata.hasmin)
            nextLayer.push(dynID);
        } else {
          //ignore items with no formula or whose formula has already been processed
          if (!itemdata.formula || Number.isNumeric(itemdata.formula) || itemdata.formula.startsWith("#") || (Number(itemdata.moddedformula) == itemdata.value && itemdata.moddedformula != ""))
            continue;

          const dynID = (item.type == "Characteristic") ? actor.slugify(itemdata.abbr) : actor.slugify(item.name);

          // the formula is a reference
          if (itemdata.formula.includes("@")) {
            const regex = /@([\w]+)/g;
            let foundAllRefs = true;
            let match;
            while ((match = regex.exec(itemdata.formula)) && foundAllRefs) {
              foundAllRefs = previousLayer.includes(match[1]);
            }
            if (foundAllRefs) {
              let formData = actor._replaceData(itemdata.formula);
              try {
                itemdata.moddedvalue = itemdata.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")) * 100) / 100;
                itemdata.moddedformula = "" + itemdata.value; // store the result as a String
              } catch (err) { // TODO: this may not be reachable now that we are checking all the refs first
                // store the formula ready to be rolled
                itemdata.moddedformula = formData.value;
                itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
                console.debug("Non-Modifier formula evaluation incomplete:\n", [itemdata.moddedformula]);
              }
              // put this item into the next layer and stop looking for matches
              nextLayer.push(dynID);
            }
          } else {
            // the formula might be a dice expression
            if (itemdata.formula.toLowerCase().includes("d")) {
              itemdata.moddedformula = itemdata.formula;
              itemdata.moddedvalue = itemdata.value = 0; // eliminate any old values
            }
          }
        }
      }
    }

    // this will store all valid modifier entries for later processing efficiency
    const activeModEntries = [];
    /**
     * Process all the modifiers after the items they use as references have
     * been calculated and the data structure for the skill and spell breakdown
     * has been created.
     */
    for (const item of actormods) {
      const itemdata = item.system;
      if (!itemdata.inEffect) continue;
      for (const entry of Object.values(itemdata.entries)) {
        if (entry.formula == "") continue;
        if (Number.isNumeric(entry.formula)) {
          entry.value = Number(entry.formula);
        } else {
          let formData = actor._replaceData(entry.formula);
          try {
            entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")));
            entry.moddedformula = "" + entry.value; // store the result as a String
          } catch (err) {
            // store the formula ready to be rolled
            entry.moddedformula = formData.value;
            entry.value = 0; // eliminate any old values
            console.debug("Modifier formula evaluation error:\n", [entry.moddedformula,]);
          }
          if (!itemdata.tempName.includes(formData.label))
            itemdata.tempName += formData.label;
        }
        activeModEntries.push(entry);
      }
    }

    /**
     * Fetch the modifiers for each rollable item and calculate the
     * moddedvalue.
     */
    for (const item of actorrollables) {
      const itemdata = item.system;
      itemdata.moddedvalue = itemdata.value;
      // if there is no modified formula, make it the value
      if (itemdata.moddedformula == undefined)
        itemdata.moddedformula = "" + itemdata.value;
      switch (item.type) {
        case "Melee-Attack":
        case "Ranged-Attack": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "attack", activeModEntries);
          break;
        }
        case "Defence": {
          itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "defence", activeModEntries);
          break;
        }
        case "Rollable": {
          switch (itemdata.category) {
            case "check": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "reaction", activeModEntries);
              break;
            }
            case "technique":
            case "skill": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "skill", activeModEntries);
              break;
            }
            case "rms":
            case "spell": {
              itemdata.moddedvalue += actor.fetchDisplayModifiers(item.name, "spell", activeModEntries);
              break;
            }
          }
          break;
        }
      }
    }
  }

  processModifierFormula(entry, itemdata) {
    if (!Number.isNumeric(entry.formula) && entry.formula != "") {
      const formData = this._replaceData(entry.formula);
      try {
        entry.value = Math.round(eval(formData.value.replace(CONFIG.system.dataRgx, "")));
        entry.moddedformula = "" + entry.value; // store the result as a String
      } catch (err) {
        // store the formula ready to be rolled
        entry.moddedformula = formData.value;
        entry.value = 0; // eliminate any old values
        console.debug("Modifier formula evaluation error:\n", [entry.moddedformula,]);
      }
      if (!itemdata.tempName.includes(formData.label)) itemdata.tempName += formData.label;
    }
  }

  async getNamedItem(name) {
    let parts = name.split(".");
    if (parts.length != 2) return undefined;
    return this.system[parts[0]][parts[1]];
  }

  slugify(text) {
    return text
      .toString() // Cast to string
      .toLowerCase() // Convert the string to lowercase letters
      .normalize("NFD") // The normalize() method returns the Unicode Normalization Form of a given string.
      .trim() // Remove whitespace from both sides of a string
      .replace(/[\s-]+/g, "_") // Replace spaces with underscore
      .replace(/[^\w]+/g, "") // Remove all non-word chars
      .replace(/\_\_+/g, "_"); // Replace multiple underscores with a single one
  }

  /**
   * Replace data references in the formula of the syntax `@attr[.fieldname =.moddedvalue]` with the corresponding key from
   * the provided `data` object.
   * @param {String} formula    The original formula within which to replace
   * @private
   */
  _replaceData(formula) {
    const dataRgx = /[@]([\w.]+)/gi;
    const dynamic = this.system.dynamic;
    const tracked = this.system.tracked;
    let tempName = "";
    const findTerms = (match, term) => {
      const fields = term.split(".");
      const attribute = fields[0];
      // default to the field "moddedvalue" if none is specified
      const field = fields[1] || "moddedvalue";
      if (dynamic[attribute]) {
        const value = dynamic[attribute].system[field];
        const label = dynamic[attribute].system.label;
        tempName += (label) ? ` (${label})` : "";
        return (value) ? String(value).trim() : "0";
      } else if (tracked[attribute]) {
        const value = tracked[attribute].system[field];
        return (value) ? String(value).trim() : "0";
      } else {
        return "0";
      }
    };
    const replyData = formula.replace(dataRgx, findTerms).replace(/(min)|(max)|(floor)|(ceil)|(round)|(abs)/g, "Math.$&");
    return { value: replyData, label: tempName };
  }

  /**
   * override to force actor update on item change
   */
  _onUpdateEmbeddedDocuments(embeddedName, ...args) {
    super._onUpdateEmbeddedDocuments(embeddedName, ...args);
    if (embeddedName !== "Item") return;
    const tokens = this.getActiveTokens(false, true);
    tokens.forEach((t) => t._onUpdateBaseActor());
  }

  // handle changes to Pools and determine their state
  async setConditions(newValue, attrName) {
    if (CONFIG.system.testMode)
      console.debug("entering setConditions()\n", [newValue, attrName]);

    const attr = attrName.split(".")[2];
    const item = this.system.tracked[attr];
    const itemdata = item.system;
    let attrValue = itemdata.value;
    let attrMax = itemdata.max;
    let attrState = itemdata.state;
    let attrMin = itemdata.min;

    // Assign the variables
    if (attrName.includes(".maxForm")) {
      attrMax = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.system.dataRgx, "")));
    } else if (attrName.includes(".minForm")) {
      attrMin = Math.round(eval(this._replaceData(newValue).value.replace(CONFIG.system.dataRgx, "")));
    } else {
      attrValue = newValue;
    }
    const ratio = attrValue / attrMax;

    switch (attr) {
      case "hp": {
        // use eighths and let the user set the minimum
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = "[1/8]";
            break;
          }
          case 2: {
            attrState = "[1/4]";
            break;
          }
          case 3: {
            attrState = "[3/8]";
            break;
          }
          case 4: {
            attrState = "[1/2]";
            break;
          }
          case 5: {
            attrState = "[5/8]";
            break;
          }
          case 6: {
            attrState = "[3/4]";
            break;
          }
          case 7: {
            attrState = "[7/8]";
            break;
          }
          case 8: {
            attrState = "[FIT]";
            break;
          }
          default: {
            // bad shape
            if (attrValue <= attrMin) {
              attrState = "[DEAD]";
            } else if (attrValue <= 0) {
              attrState = "[UNC]";
            } else {
              attrState = "[< 1/8]";
            }
          }
        }
        break;
      }
      default: {
        // assume that the state is measured in eighths
        switch (Math.trunc(ratio * 8)) {
          case 1: {
            attrState = "[1/8]";
            break;
          }
          case 2: {
            attrState = "[1/4]";
            break;
          }
          case 3: {
            attrState = "[3/8]";
            break;
          }
          case 4: {
            attrState = "[1/2]";
            break;
          }
          case 5: {
            attrState = "[5/8]";
            break;
          }
          case 6: {
            attrState = "[3/4]";
            break;
          }
          case 7: {
            attrState = "[7/8]";
            break;
          }
          case 8: {
            attrState = "[Full]";
            break;
          }
          default: {
            // dead
            if (ratio <= 0) {
              // empty
              attrState = "[Empty]";
            } else {
              attrState = "[< 1/8]";
            }
          }
        }
      }
    }
    itemdata.min = attrMin;
    itemdata.value = attrValue;
    itemdata.max = attrMax;
    itemdata.state = attrState;

    return await item.update({ system: itemdata });
  }

  /**
   * Handle how changes to a Token attribute bar are applied to the Actor.
   * Logic for pools to be edited by plus-minus and text input (on Actor and Item) is also redirected here.
   * @param {string} attribute    The attribute path
   * @param {number} value        The target attribute value
   * @param {boolean} isDelta     Whether the number represents a relative change (true) or an absolute change (false)
   * @param {boolean} isBar       Whether the new value is part of an attribute bar, or just a direct value
   * @return {Promise}
   */
  async modifyTokenAttribute(attribute, value, isDelta = false, isBar = true) {
    if (CONFIG.system.testMode) console.debug("entering modifyTokenAttribute()\n", [attribute, value, isDelta, isBar,]);

    if (isBar && !attribute.endsWith(".system"))
      attribute += ".system";
    // fetch the pool if the attribute changing is 'value', or the attribute itelf if not
    const current = getProperty(this.system, attribute);
    // isBar is true for the value and must be clamped
    // isBar is false for the min or max
    value = isBar
      ? Math.clamped(current.min, isDelta ? Number(current.value) + Number(value) : Number(value), current.max)
      : isDelta ? Number(current) + Number(value) : value;

    // redirect updates to setConditions
    return await this.setConditions(value, `system.${attribute}`);
  }

  /**
   * Fetches the formatted modifiers for a roll.
   */
  fetchRollModifiers(actordata, dataset) {
    let tempMods = [];
    const actormods = actordata.items.filter(i => (i.type == "Modifier"));
    const type = (dataset.type == "modlist") ? dataset.modtype : dataset.type;

    // preload gmod
    tempMods.push({ modifier: Number(actordata.system.gmod.value) || 0, description: 'global modifier' });

    // iterate through the modifiers in effect, picking the appropriate ones for the roll
    for (const mod of actormods) {
      const moddata = mod.system;

      if (moddata.inEffect) {

        for (let entry of Object.values(moddata.entries)) {
          // check to see if this entry applies to this type of roll
          switch (type) {
            case "technique":
              if (entry.category != "skill") continue;
              break;
            case "rms":
              if (entry.category != "spell") continue;
              break;
            case "dodge":
            case "block":
            case "parry":
              if (entry.category != "defence") continue;
              break;
            default:
              if (entry.category != type) continue;
          }
          if (Number(entry.moddedformula) == 0) continue;

          // what is the target of this modifier?
          let hasRelevantTarget = entry.targets.trim();
          // does this modifier have a target matching the item being rolled?
          if (hasRelevantTarget != "") {
            hasRelevantTarget = false;
            // get the array of targets to which it applies
            const cases = entry.targets.split(",").map(word => word.trim());
            for (const target of cases) {
              // test the target against the beginning of dataset.name for a match
              if (dataset.name.startsWith(target)) {
                hasRelevantTarget = true;
                continue;
              }
            }
          } else {
            // a general modifier
            hasRelevantTarget = true;
          }

          if (hasRelevantTarget) {
            let clean = entry.moddedformula?.replace(/\(([-+][\d]+)\)/g, "$1");
            tempMods.push({
              modifier: (clean == undefined) ? entry.value : clean[0] == "+" || clean[0] == "-" ? clean : "+" + clean,
              description: moddata.tempName
            });
          }
        }
      }
    }
    const prepareModList = mods => mods.map(mod => ({ ...mod, modifier: mod.modifier })).filter(mod => mod.modifier !== 0);
    return prepareModList(tempMods);
  }

  /**
   * Fetches the total modifier value for a rollable item
   */
  fetchDisplayModifiers(name, type, activeModEntries) {
    let mods = 0;
    const modEntries = activeModEntries.filter((ame) => ame.category == type);
    for (const entry of modEntries) {
      // what is the target of this modifier?
      let hasRelevantTarget = entry.targets.trim();
      // does this modifier have a target matching the item being rolled?
      if (hasRelevantTarget != "") {
        hasRelevantTarget = false;
        // get the array of targets to which it applies
        const cases = entry.targets.split(",").map((word) => word.trim());
        for (const target of cases) {
          // test the target against the beginning of dataset.name for a match
          if (name.startsWith(target)) {
            hasRelevantTarget = true;
            continue;
          }
        }
      } else {
        // a general modifier
        hasRelevantTarget = true;
      }
      mods += hasRelevantTarget ? entry.value : 0;
    }
    return mods;
  }

  /**
   * Resets all temporary Variables and Modifiers
   */
  resetModVars(temporary = true) {
    const items = this.items.filter(function (item) {
      if (temporary) {
        return item.system.temporary;
      } else {
        return item.system.once;
      }
    });
    let updates = [];

    for (const item of items) {
      switch (item.type) {
        case "Modifier": {
          updates.push({ _id: item.id, ["system.inEffect"]: false, });
          break;
        }
        case "Variable": {
          updates.push({
            _id: item.id,
            ["system.value"]: item.system.entries[1].value,
            ["system.formula"]: item.system.entries[1].formula,
            ["system.label"]: item.system.entries[1].label,
          });
          break;
        }
      }
    }
    this.updateEmbeddedDocuments("Item", updates);
  }

  /**
   * rolldata consists of:
   * - name: the name of the roll
   * - type: the type of roll (used for filtering modifiers)
   * - roll: the value being processed by this function
   * - modtype: if the type is "modlist" then this is the type of the roll
   * - id: the _id of the item being rolled, so we can access all it's raw data
   */
  async roll(rolldata) {

    let flavour = rolldata.flavour || "";
    let tempflavour = "";
    const actor = rolldata.actor;
    const actordata = actor.system;
    const item = await actor.items.get(rolldata.id);
    if (!item && "attackskill".includes(rolldata.type)) return ui.notifications.error(`No Item identified.`);

    let isModList = false;
    let hideTotal = false;
    let benefits = 0;
    let hardships = 0;
    let prerollmods = 0;
    let tokeep = 0;

    const rolledInitiative = rolldata.name == "Initiative";

    switch (rolldata.type) {
      case "tochat":
      case "technique": { // send the notes to the chat and return
        ChatMessage.create({
          speaker: ChatMessage.getSpeaker({ actor: actor }),
          flavor: `<b>${rolldata.name}</b><hr>`,
          content: rolldata.roll.replace(/(?:\r\n|\r|\n)/g, '<br>'),
        });
        return;
      }
      case "rms": { // no roll, just storing a value
        return;
      }
      case "attack": {
        const attack = item;
        const attackdata = attack.system;
        const ammopool = actordata.tracked[attackdata.minST.toLowerCase()];
        if (ammopool) {
          // has ammunition
          const ammoused = Math.min(attackdata.accuracy, ammopool.system.value);
          // Decrement Pool by the Rate of Fire value
          await actor.updateEmbeddedDocuments("Item", [{
            _id: ammopool._id,
            system: { value: Math.max(0, ammopool.system.value - ammoused) }
          }]);
          // Get remaining ammo count post-attack to display in output
          const ammo_left = ammopool.system.value;
          tempflavour += `<hr><b>Range:</b> ${attackdata.range}<br>`;
          if (ammoused == 0) {
            tempflavour += `<b>Ammo used:</b> ${ammoused} <span class="critfail">Click! Attack failed! Reload!</span><br>`;
          } else if (ammo_left == 0) {
            tempflavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left}) <span class="critfail">Reload!</span><br>`;
          } else {
            tempflavour += `<b>Ammo used:</b> ${ammoused} (Remaining: ${ammo_left})<br>`;
          }
          tempflavour += `<b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
        } else {
          if (attack.type == "Ranged-Attack") {
            tempflavour += `<hr><b>Range:</b> ${attackdata.range}<br><b>Damage:</b><span class="rollable" data-item-id="{{item._id}}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          } else {
            // a melee attack
            tempflavour += `<hr><b>Damage:</b><span class="rollable" data-item-id="${item._id}" data-name="${attack.name}" data-roll="${attackdata.damage}" data-type="damage"> ${attackdata.damage} - ${attackdata.damageType}</span><hr><b>Strike Modifiers:</b><br>Base:  `;
          }
        }
        break;
      }
    }
    // add the item-id to the flavour
    tempflavour += `<span data-item-id="${item?.id}"></span>`;

    // get the modifiers
    const modList = actor.fetchRollModifiers(actor, { type: rolldata.type, modtype: rolldata.modtype, name: rolldata.name });
    // process the modifiers
    let modformula = "";
    flavour += (rolldata.type != "modlist") ? ` [<b>${rolldata.roll}</b>]` : `<p class="chatmod">[<b>${rolldata.roll}</b>]: ${rolldata.name}<br>`;
    const hasMods = (modList.length != 0);
    if (hasMods) {
      var sign;
      tempflavour += (rolldata.type != "modlist") ? `<p class="chatmod">` : "";
      for (const mod of modList) {
        if (typeof mod.modifier == 'string') { // benefit, hardship or dice expression
          let occurrences = [...mod.modifier].reduce((a, e) => { a[e] = a[e] ? a[e] + 1 : 1; return a }, {});
          if (occurrences.b) {
            benefits += occurrences.b;
            tempflavour += ` ${benefits} BEN : ${mod.description} <br>`;
          }
          if (occurrences.h) {
            hardships += occurrences.h;
            tempflavour += ` ${hardships} HSP : ${mod.description} <br>`;
          }
          if (occurrences.d) {
            modformula += `${mod.modifier}`;
            tempflavour += ` ${mod.modifier} : ${mod.description} <br>`;
          }
        } else { // numerical modifier
          sign = mod.modifier > -1 ? "+" : "";
          prerollmods += mod.modifier;
          tempflavour += ` ${sign}${mod.modifier} : ${mod.description} <br>`;
        }
      }
      tempflavour += `</p>`;
    }

    var formula = "";
    switch (rolldata.type) {
      case "modlist": // this will render a dialog with the modifiers, not a chat message
        isModList = true;
      case "check": { // a valid dice expression
        formula = rolldata.roll;
        break;
      }
      case "skill": {
        const isattribute = item.type == "Attribute";
        const untrained = isattribute || item.system.value == 0;

        const attribute = isattribute ? null : actor.system.dynamic[item.system.attribute];
        const attr = isattribute ? item.system.moddedvalue : attribute.system.moddedvalue;
        const max = attr * 2;
        const sk = isattribute ? 0 : item.system.moddedvalue;

        flavour += isattribute ? `` : ` under <b>${attribute.system.abbr} [${attr}]</b>`;
        flavour += tempflavour;

        let toroll = attr + sk + benefits;
        tokeep = attr;
        let unresolved = true;
        while (unresolved) {
          console.log(`r${toroll}, k${tokeep}, mod${prerollmods}, - hsp${hardships}`);
          if (toroll > tokeep * 2) {
            if (tokeep < max) {
              if (toroll > (tokeep * 2) + 1) {
                toroll -= 2;
                tokeep++;
              } else {
                toroll--;
                prerollmods += 3;
              }
            } else {
              toroll--;
              prerollmods += 3;
            }
          } else if (toroll > max) {
            if (tokeep < max) {
              if (toroll > (tokeep * 2) + 1) {
                toroll -= 2;
                tokeep++;
              } else {
                toroll--;
                prerollmods += 3;
              }
            } else {
              toroll--;
              prerollmods += 3;
            }
          } else {
            unresolved = false;
          }
        }
        formula = `${toroll}d10${untrained ? '' : 'x10'}k${tokeep}
        ${prerollmods == 0 ? '' : prerollmods > -1 ? `+${prerollmods}` : `${prerollmods}`}
        ${hardships > 0 ? `-${hardships}d10x` : ''}`;
        flavour += (untrained) ? `<p class="chatmod critfail">Untrained</p>` : '';
        break;
      }
      default: { // there should not be any of these
        console.warn("This type of roll is not yet supported: " + rolldata.type);
      }
    }

    // process the modified roll
    let roll = await new Roll(formula).evaluate({ async: true });

    if (isModList) { // render the modlist dialog and return
      flavour += tempflavour + `<hr><p>${roll.result} = <b>${roll.total}</b></p>`;
      new Dialog({
        title: actor.name,
        content: flavour,
        buttons: {
          close: {
            label: "Close"
          },
        },
        default: "close"
      }).render(true)
      return;
    }
    if (rolldata.type == "block") { // send the modified value to the chat and return
      flavour += tempflavour + ` <hr>Total: [<b>${roll.total}</b>]`;
      ChatMessage.create({
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        content: flavour
      });
      return;
    }

    if (rolldata.type != "check") {
      const mainrollobject = roll.toJSON();
      mainrollobject.evaluated = false;
      const results = mainrollobject.terms[0].results;
      const newresults = [];
      for (let die = 0; die < results.length; die++) {
        const current = { ...results[die] };
        if (current.exploded) {
          while (results[++die].result == 10) {
            current.result += 10;
          }
          current.result += results[die].result;
          newresults.push(current);
        } else {
          newresults.push(current);
        }
      }
      newresults.sort((a, b) => b.result - a.result);
      for (let die = 0; die < newresults.length; die++) {
        const current = newresults[die];
        if (die < tokeep) {
          current.active = true;
          delete current.discarded;
        } else {
          current.active = false;
          current.discarded = true;
        }
      }
      mainrollobject.terms[0].results = newresults;
      roll = await Roll.fromData(mainrollobject).evaluate({ async: true });
    }

    // prepare the flavor in advance based on which type of die is in use.
    roll.toMessage(
      {
        speaker: ChatMessage.getSpeaker({ actor: actor }),
        flavor: flavour,
        flags: { hideMessageContent: hideTotal }
      }
    );
    let updates = { ["system.gmod.value"]: 0 };
    if (rolledInitiative) {
      updates["system.bs.value"] = roll.total;
    }
    actor.resetModVars(false);
    actor.update(updates);
  }

  rollables(macroData) {
    macroData.position = macroData.position || { width: 300, top: 0, left: 0 };
    macroData.type = macroData.type || "";
    macroData.category = macroData.category || "";
    macroData.group = macroData.group || "";
    macroData.target = macroData.target || "";
    macroData.flavour = macroData.flavour || "";

    const rollables = this.items
      .filter(function (item) {
        if ("ContainerTraitPoolModifierVariableCharacteristicEquipment".includes(item.type)) return null;
        if (macroData.target != "") {
          const cases = macroData.target.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.name.includes(target))) return null;
        }
        if (macroData.group != "") {
          const cases = macroData.group.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.group.includes(target))) return null;
        }
        if (macroData.category != "") {
          const cases = macroData.category.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.system.category?.includes(target))) return null;
        } else {
          const cases = macroData.type.split(",").map((word) => word.trim());
          if (!cases.some((target) => item.type.includes(target))) return null;
        }
        return item;
      })
      .sort((a, b) => {
        if (a.name < b.name) return -1;
        if (a.name > b.name) return 1;
        return 0;
      });

    switch (rollables.length) {
      case 0:
        ui.notifications.error("Your rollmacro call produced no results");
        break;
      case 1: // go straight to the roll dialog with modifiers
        this.modifierdialog(rollables[0]._id, macroData);
        break;
      default: // provide the choice of results
        this.rollabledialog(rollables, macroData);
        break;
    }
  }

  async rollabledialog(rollabledata, macroData) {
    const actor = this;
    const template = "systems/hnh/templates/dice/rollables.hbs";
    const html = await renderTemplate(template, { rollabledata });

    new Dialog(
      {
        title: "Rollables",
        content: html,
        buttons: {},
        options: {
          actor: actor,
          macroData: macroData,
        },
      },
      {
        width: macroData.position.width || 300,
        top: macroData.position.top || 0,
        left: macroData.position.left || 0,
      }
    ).render(true);
  }

  // a test method for executing token actions. SysDev part 8 covers a better way to do this.
  async modifierdialog(rollableId, macroData) {
    const rollabledata = this.items.get(rollableId);
    const actor = this;
    const name = rollabledata.name;
    const type = rollabledata.type;
    const category = rollabledata.system.category || null;

    const modifiers = [];
    for (const mod of Object.values(this.system.modifiers)) {
      for (let entry of Object.values(mod.system.entries)) {
        if (entry.formula == "") continue;
        // check to see if this entry applies to this type of roll
        switch (type) {
          case "Melee-Attack":
          case "Ranged-Attack": {
            if (!["damage", "attack"].includes(entry.category)) continue;
            break;
          }
          case "Defence":
          case "Rollable": {
            if (category != entry.category) continue;
            break;
          }
          case "Attribute": {
            if (!entry.targets.toLowerCase().includes(rollabledata.system.abbr.toLowerCase())) continue;
            break;
          }
        }

        let hasRelevantTarget = entry.targets.trim();
        // does this modifier have targets?
        if (hasRelevantTarget != "") {
          hasRelevantTarget = false;
          // get the array of targets to which it applies
          const cases = entry.targets.split(",").map((word) => word.trim());
          for (const target of cases) {
            // test the target against the beginning of name for a match
            if (name.startsWith(target)) {
              hasRelevantTarget = true;
              continue;
            }
          }
        } else {
          // a general modifier
          hasRelevantTarget = true;
        }
        if (hasRelevantTarget) {
          // is the modifier already in the list?
          if (modifiers.includes(mod)) continue;
          // does the modifier have a reference?
          if (entry.formula.includes("@")) {
            // is the reference to a Variable or Pool?
            const regex = /@([\w.\-]+)/gi;
            const reference = regex.exec(entry.formula)[1];
            const varpool = actor.system.tracked[reference] || actor.system.dynamic[reference];
            if (varpool) {
              if (!modifiers.includes(varpool)) modifiers.push(varpool);
            }
          }
          modifiers.push(mod);
        }
      }
    }
    // add withAdvantage to every dialog
    const varpool = actor.system.dynamic.withadvantage;
    if (varpool) {
      if (!modifiers.includes(varpool)) modifiers.push(varpool);
    }

    // Is there at least one target selected so we can fetch it's defence data?
    const hasTarget = game.user.targets.size > 0 && (type == "Melee-Attack" || type == "Ranged-Attack");
    const target = game.user.targets.first()?.name || "No Target";

    const template = "systems/hnh/templates/dice/modifiers.hbs";
    const html = await renderTemplate(template, { // can add target data here too (see VsD)
      actor,
      rollabledata,
      modifiers,
      hasTarget,
      target
    });

    new Dialog(
      {
        title: "Rollable",
        content: html,
        buttons: {
          back: {
            label: "Back",
            callback: (html) => {
              this.rollables(macroData);
            },
          },
        },
        options: {
          actor: actor,
          macroData: macroData,
        },
      },
      {
        width: macroData.position.width || 300,
        top: macroData.position.top || 0,
        left: macroData.position.left || 0,
      }
    ).render(true);
  }
}
