/**
 * Extend the basic ActorSheet with some very simple modifications
 * @extends {ActorSheet}
 */
export class BaseActorSheet extends ActorSheet {

  itemContextMenu = [
    {
      name: game.i18n.localize("local.sheet.edit"),
      icon: '<i class="fas fa-edit"></i>',
      callback: (element) => {
        const item = this.actor.items.get(element.data("item-id"));
        item.sheet.render(true);
      }
    },
    {
      name: game.i18n.localize("local.sheet.delete"),
      icon: '<i class="fas fa-trash"></i>',
      callback: (element) => {
        this.actor.deleteEmbeddedDocuments("Item", [element.data("item-id")]);
      }
    }
  ];

  /** @override */
  getData() {
    if (CONFIG.system.testMode) console.debug("entering getData() in baseActor-sheet\n", this);
    // why am I not doing this?
    //const sheetData = super.getData();

    // Basic data from dnd5e.mjs#10732
    let isOwner = this.actor.isOwner;
    const rollData = this.actor.getRollData.bind(this.actor);
    const context = {
      owner: isOwner,
      limited: this.actor.limited,
      options: this.options,
      editable: this.isEditable,
      cssClass: isOwner ? "editable" : "locked",
      isCharacter: this.actor.type === "Trooper",
      isNPC: this.actor.type === "npc",
      isVehicle: this.actor.type === "vehicle",
      config: CONFIG.system,
      rollData
    };
    context.enrichment = {
      system: {
        biography: TextEditor.enrichHTML(this.actor.system.biography, { async: false, secrets: this.actor.isOwner, relativeTo: this.actor })
      }
    };

    // The Actor's data
    const actor = this.actor;
    const actordata = actor.toObject(false);
    context.actor = actordata;
    context.system = actordata.system;
    context.items = actordata.items.sort((a, b) => {
      if (a.name < b.name) return -1;
      if (a.name > b.name) return 1;
      return 0;
    });

    // My shortcuts
    const myData = context.system;
    const dynamic = myData.dynamic;
    const tracked = myData.tracked;

    myData.equipment = [];
    myData.spells = [];
    myData.checks = [];
    myData.traits = [];
    myData.advantages = [];
    myData.disadvantages = [];
    myData.perks = [];
    myData.quirks = [];
    myData.attacks = [];
    myData.defences = [];
    myData.pools = [];
    myData.containers = [];
    myData.modifiers = [];
    myData.variables = [];
    myData.attackdamagemods = [];
    myData.defencemods = [];
    myData.reactionmods = [];
    myData.skillmods = [];
    myData.spellmods = [];
    myData.checkmods = [];
    myData.attributemods = [];

    for (const item of context.items) {
      const itemdata = item.system;
      switch (item.type) {
        case "Equipment": {
          myData.equipment.push(item);
          break;
        }
        case "Rollable": {
          itemdata.displaycategory = game.i18n.localize(`local.item.rollable.${itemdata.category}`);
          switch (itemdata.category) {
            case "skill":
            case "technique": {
              myData.checks.push(item);
              break;
            }
            case "spell":
            case "rms": {
              myData.spells.push(item);
              break;
            }
            case "check": {
              myData.checks.push(item);
              break;
            }
          }
          break;
        }
        case "Power": {
          myData.checks.push(item);
          break;
        }
        case "Trait": {
          myData.traits.push(item);
          itemdata.displaycategory = game.i18n.localize(`local.item.trait.${itemdata.category}`);
          if (itemdata.group.split(" ")[0].toLowerCase() != "notrait") {
            switch (itemdata.category) {
              case "advantage": {
                myData.advantages.push(item);
                break;
              }
              case "disadvantage": {
                myData.disadvantages.push(item);
                break;
              }
              case "perk": {
                myData.perks.push(item);
                break;
              }
              case "quirk": {
                myData.quirks.push(item);
                break;
              }
            }
          }
          break;
        }
        case "Ranged-Attack":
        case "Melee-Attack": {
          myData.attacks.push(item);
          break;
        }
        case "Defence": {
          myData.defences.push(item);
          break;
        }
        case "Pool": {
          myData.pools.push(item);
          break;
        }
        case "Container": {
          myData.containers.push(item);
          break;
        }
        case "Variable": {
          myData.variables.push(item);
          break;
        }
        case "Modifier": {
          myData.modifiers.push(item);
          if (itemdata.attack || itemdata.damage) myData.attackdamagemods.push(item);
          if (itemdata.defence) myData.defencemods.push(item);
          if (itemdata.reaction) myData.reactionmods.push(item);
          if (itemdata.skill) myData.skillmods.push(item);
          if (itemdata.spell) myData.spellmods.push(item);
          if (itemdata.check) myData.checkmods.push(item);
          if (itemdata.attribute) myData.attributemods.push(item);
          break;
        }
      }
      if (item.type != "Trait") {// For non-trait items to be displayed with them
        // add the item to the trait list
        switch (itemdata.group.split(" ")[0].toLowerCase()) {
          case "spell":
          case "advantage": {
            myData.advantages.push(item);
            break;
          }
          case "race":
          case "disadvantage": {
            myData.disadvantages.push(item);
            break;
          }
          case "character":
          case "perk": {
            myData.perks.push(item);
            break;
          }
          case "job":
          case "quirk": {
            myData.quirks.push(item);
            break;
          }
        }
      }
    }

    // filter items in All Items
    let filterstring = "";
    for (const filter of Object.entries(myData.allitems)) {
      if (filter[1]) filterstring += filter[0];
    }
    if (filterstring == "") {
      context.filtereditems = context.items;
    } else {
      context.filtereditems = context.items.filter(function (item) {
        if (filterstring.includes(item.type)) return item;
        return null;
      });
    }

    return context;
  }

  /** @override */
  activateListeners(html) {
    super.activateListeners(html);

    html.find('.item-create').click(this._onItemCreate.bind(this));
    html.find('.item-export').click(this._onItemExport.bind(this));
    html.find('.importJSON').click(this._onImportJSON.bind(this));

    html.find('.iteminput').change(this._onInputChange.bind(this));
    html.find('.plus').click(this._onPlusMinus.bind(this));
    html.find('.minus').click(this._onPlusMinus.bind(this));
    html.find('.rollable').click(this._onRoll.bind(this));
    html.find('.rollmacro').click(this._rollmacro.bind(this));
    html.find('.item-edit').click(this._onItemEdit.bind(this));
    html.find('.item-delete').click(this._onItemDelete.bind(this));
    html.find('.item-toggle').click(this._onToggleItem.bind(this));
    html.find('.collapsible').click(this._onToggleCollapse.bind(this));

    new ContextMenu(html, ".item", this.itemContextMenu);

    const handler = ev => this._onDragStart(ev);
    html.find('.draggable').each((i, li) => {
      li.setAttribute("draggable", true);
      li.addEventListener("dragstart", handler, false);
    });

    html.on('click', '.filter', ev => {
      ev.preventDefault();
      const type = ev.currentTarget.dataset.type;
      const current = this.actor.system.allitems[type];
      const update = `system.allitems.${type}`;
      this.actor.update({ [update]: !current });
    });

    html.on('click', '.open-journal', ev => {
      ev.preventDefault();
      const journal = ev.currentTarget.dataset.table;
      try {
        Journal._showEntry(game.journal.getName(journal).uuid, "text", true);
      } catch (err) {
        if (journal) ui.notifications.error(`There is no matching Journal Entry for ${journal}. Perhaps you have yet to import it from the compendium?`);
      }
    });
  }

  /**
   * Each field is a comma-separated list of the things you want
   * to include in the result. It matches against the text you
   * enter, so a category of "s" will match skill, spell and rms.
   *
   * type: may be any of {Primary, Melee, Ranged, 
   * Attack, Rollable, Defence}
   *
   * category: applies only to:
   * Rollable: {check, skill, spell, technique, rms} and
   * Defence: {dodge, parry, block}
   * and may be used without specifying the type.
   *
   * group: is a user-defined field for use in filtering
   * this result.
   *
   * target: Any case-sensitive portion of the item name.
  */
  _rollmacro(ev) {
    const options = {
      position: {
        width: 300,
        top: 100,
        left: 100
      },
      type: ev.currentTarget.dataset.type || "",
      category: ev.currentTarget.dataset.category || "",
      group: ev.currentTarget.dataset.group || "",
      target: ev.currentTarget.dataset.target || "",
      damage: ev.currentTarget.dataset.damage == "damage" || false
    };
    const tokens = game.canvas.tokens.controlled;
    if (tokens.length != 0) { // use the token's actor just in case they are not linked
      if (tokens[0].actor.id == this.actor.id) tokens[0].actor.rollables(options);
    } else {
      this.actor.rollables(options);
    }
  }

  async _onRoll(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;

    const rolldata = {
      actor: this.actor,
      flavour: game.i18n.format(`local.phrases.${dataset.type}`, { name: dataset.name, }) || "",
      name: dataset.name || "",
      roll: dataset.roll || "",
      type: dataset.type || "",
      modtype: dataset.modtype || "",
      id: dataset.itemId
    };

    this.actor.roll(rolldata);
  }

  async _onImportJSON(event) {
    event.preventDefault();
    let scriptdata = this.actor.system.itemscript || "";
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata
      .replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    const data = JSON.parse(scriptdata);
    console.log("JSON Object\n", data);
  }

  _onToggleCollapse(event) {
    event.preventDefault();
    if (CONFIG.system.testMode) console.debug("entering _onToggleCollapse()", [this, event]);
    // do not collapse when creating an item
    if (event.target.parentElement.className.includes("item-create")) return;
    if (event.target.parentElement.className.includes("char-name")) return;
    const section = event.currentTarget.dataset.section;
    const current = this.actor.system.sections[section];
    const update = `system.sections.${section}`;
    const data = current === "block" ? "none" : "block";
    this.actor.update({ [update]: data });
  }

  async _onPlusMinus(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onPlusMinus()", [this, event]);

    const field = event.currentTarget.firstElementChild;
    const actordata = this.actor.system;
    const fieldName = field.name;
    const change = Number(field.value);
    var value;
    var fieldValue;

    if (field.className.includes("pool")) {
      return this.actor.modifyTokenAttribute(`tracked.${fieldName.toLowerCase()}`, field.value, true, true);

    } else { // haven't figured out non-pools yet

      switch (fieldName) {
        case "gmod": {
          fieldValue = "system.gmod.value";
          value = change + actordata.gmod.value;
          this.actor.update({ [fieldValue]: value });
          break;
        }
        default: {
          break;
        }
      }
    }
  }

  _onToggleItem(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    let attr = "";
    switch (item.type) {
      case "Modifier": {
        attr = "system.inEffect";
        break;
      }
      default: {
        ui.notifications.warning(`Toggling of ${item.type} is not yet supported.`)
      }
    }
    return this.actor.updateEmbeddedDocuments("Item", [{ _id: itemId, [attr]: !getProperty(item, attr) }]);
  }

  _onItemEdit(event) {
    event.preventDefault();
    const itemId = event.currentTarget.closest(".item").dataset.itemId;
    const item = this.actor.items.get(itemId);
    item.sheet.render(true);
  }

  _onItemDelete(event) {
    event.preventDefault();
    const element = event.currentTarget;
    const itemId = element.closest(".item").dataset.itemId;
    this.actor.deleteEmbeddedDocuments("Item", [itemId]);
  }

  async dropItem(item) {
    // special container behaviour

    // the item has already been created on the actor sheet
    const baseitem = await this.actor.items.get(item[0].id);

    if (baseitem.type == "Container") {
      // unpack the container then delete it
      await this._createItems(baseitem.system.notes);
      this.actor.deleteEmbeddedDocuments("Item", [item[0].id]);
    } else {
      // show the item sheet
      // FIXME: don't open an item sheet that originated on the same actor sheet
      baseitem.sheet.render(true);
    }
    return baseitem;
  }

  /** @override */
  async _onDrop(event) {
    // Try to extract the data
    let transferData;
    try {
      transferData = JSON.parse(event.dataTransfer.getData("text/plain"));
    } catch (err) {
      return false;
    }
    const actor = this.actor;
    const sameActor =
      transferData.actorId === actor.id ||
      (actor.isToken && transferData.tokenId === actor.token.id);

    // wait for the item to be copied to the actor
    const item = await super._onDrop(event);

    if (sameActor) return item;
    if (item) {
      // a proper item was dropped and identified so unpack it or render it
      return this.dropItem(item);
    } else {
      // a piece of non-item data was dropped
      const dragData = event.dataTransfer.getData("text/plain");
      this.dropData(JSON.parse(dragData));
    }
  }

  async _onTokenDrop(dragItem) {
    // wait for the item to be copied to the actor
    if (dragItem.type == "Item") {
      const item = await this._onDropItem(null, dragItem);
      return this.dropItem(item);
    } else {
      this.dropData(dragItem);
    }
  }

  _onInputChange(event) {
    event.preventDefault();

    if (CONFIG.system.testMode) console.debug("entering _onInputChange()", [this, event]);

    const target = event.currentTarget;
    // get the item id
    const itemId = target.parentElement.attributes["data-item-id"]?.value;
    // get the name of the changed element
    const dataname = target.attributes["data-name"].value;
    // get the new value
    const value = target.type === "checkbox" ? target.checked : target.value;
    // Get the original item.
    const item = this.actor.items.get(itemId);

    if (dataname == "system.alwaysOn") {
      item.update({ "system.alwaysOn": value, "system.inEffect": true });
      return;
    }

    // redirect changes to pool values
    if (target.className.includes("pool")) {
      const isDelta = value.startsWith("+") || value.startsWith("-");
      return this.actor.modifyTokenAttribute(`tracked.${this.actor.slugify(target.dataset.abbr)}`, value, isDelta, true);
    }
    // update the item with the new value for the element or the actor if it is gmod
    item ? item.update({ [dataname]: value }) : this.actor.update({ [dataname]: value });
  }

  _onItemCreate(event) {
    event.preventDefault();
    const dataset = event.currentTarget.dataset;
    const scriptdata = this.actor.system.itemscript || "";
    if (!dataset.type) {
      this._createItems(scriptdata);
    } else {
      this._createItem(dataset);
    }
  }

  _onItemExport() {
    const items = duplicate(this.actor.system.items);
    var exported = [];
    for (const item of items) {
      if (item.type == "Container") continue;
      delete item._id;
      delete item.flags;
      delete item.sort;
      delete item.effects;
      exported.push(item);
    }
    exported = JSON.stringify(exported);
    this.actor.update({ "system.itemscript": exported });
  }

  async _createItems(scriptdata) {
    // preserve newlines, etc - use valid JSON
    scriptdata = scriptdata
      .replace(/\\n/g, "\\n")
      .replace(/\\'/g, "\\'")
      .replace(/\\"/g, '\\"')
      .replace(/\\&/g, "\\&")
      .replace(/\\r/g, "\\r")
      .replace(/\\t/g, "\\t")
      .replace(/\\b/g, "\\b")
      .replace(/\\f/g, "\\f");
    // remove non-printable and other non-valid JSON chars
    scriptdata = scriptdata.replace(/[\u0000-\u0019]+/g, "");
    await this.actor.createEmbeddedDocuments("Item", JSON.parse(scriptdata));
    await this.actor.update({ "system.itemscript": "" });
    ui.notifications.info("Creation of multiple items completed!");
  }

  async _createItem(dataset) {
    const source = {
      name: dataset.type,
      type: dataset.type,
      system: {},
    };
    if (dataset.category) source.system.category = dataset.category;
    if (dataset.type == "Modifier") {
      source.system.attack = dataset.mod1 == "attack" || dataset.mod2 == "attack";
      source.system.defence =
        dataset.mod1 == "defence" || dataset.mod2 == "defence";
      source.system.skill = dataset.mod1 == "skill" || dataset.mod2 == "skill";
      source.system.spell = dataset.mod1 == "spell" || dataset.mod2 == "spell";
      source.system.check = dataset.mod1 == "check" || dataset.mod2 == "check";
      source.system.reaction =
        dataset.mod1 == "reaction" || dataset.mod2 == "reaction";
      source.system.damage = dataset.mod1 == "damage" || dataset.mod2 == "damage";
      source.system.attribute =
        dataset.mod1 == "attribute" || dataset.mod2 == "attribute";
    }
    return await this.actor.createEmbeddedDocuments("Item", [source], {
      renderSheet: true,
    });
  }
}