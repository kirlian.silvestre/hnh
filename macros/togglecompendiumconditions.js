/**
 * This will bring up a dialog containing the names of all the conditions in 
 * the specified items compendium. They must all be modifiers that may be toggled 
 * and you use that dialog to toggle the effect on selected tokens. This macro 
 * will copy the condition to the token before toggling it so you will always 
 * apply the most recent version of the condition. It will also remove it when 
 * toggling it off to ensure you don't use the wrong one manually.
 */

const compendiumID = "hnh.conditions"; // the system compendium
//const compendiumID = "world.conditions"; // define your own in a world compendium

/**
 * @compendiumID is the lowercase identifier of the compendium containing your conditions
 */
async function toggleFolderConditions(compendiumID) {
  const compendium = game.packs.get(compendiumID);
  if (compendium == undefined) {
    ui.notifications.error(`The folder <b>${compendiumID}</b> was not found.`);
    return;
  }

  const items = await compendium.getDocuments();

  const buttons = {};
  for (const button of items) {
    buttons[button.name] = {
      label: button.name,
      buttonData: button,
      callback: () => {
        (label = button.name), (icon = button.img), d.render(true);
      },
    };
  }

  let d = new Dialog(
    {
      title: `Set Status Effect`,
      buttons: buttons,
      close: async (html) => {
        if (icon) {

          const tokens = canvas.tokens.controlled;
          if (tokens.length == 0) {
            ui.notifications.error("Please select at least one token first");
            return;
          }
          for (const token of tokens) {
            await token.toggleEffect({
              icon: icon,
              id: label,
              label: label,
            });
            const actor = token.actor;
            const olditems = [];
            const itemdata = buttons[label].buttonData;
            // if there is already an item of this name and type, delete it
            const existing = actor.items.filter((item) => item.name == itemdata.name);
            while (existing[0]) {
              const current = existing.pop();
              if (current.type == itemdata.type) olditems.push(current.id);
            }
            await actor.deleteEmbeddedDocuments("Item", olditems);

            const inEffect = actor.effects.find((i) => i.label == label) != undefined;
            if (inEffect) {
               await actor.createEmbeddedDocuments("Item", [itemdata], { renderSheet: false, });
               const newitem = actor.items.filter((item) => item.name == itemdata.name)[0];
               await actor.updateEmbeddedDocuments("Item", [{ _id: newitem._id, "system.inEffect": true },]);
            }
          }
          (label = ""), (icon = "");
        }
      },
    },
    {
      width: 200,
      classes: ["mydialog"],
      top: 0,
      left: 0,
    }
  );
  d.render(true);
}
toggleFolderConditions(compendiumID);
console.log(compendiumID);