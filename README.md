# Heroes & Hardships

The Heroes & Hardships Universal RPG System by Earl of Fife Games LLC.

## Description

This system is a game aid for playing Heroes & Hardships on Foundry VTT.

This game aid is the original creation of James Huddleston (https://www.youtube.com/channel/UC-WeR6LCq_UuPpldldbGDvw).
