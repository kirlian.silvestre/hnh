// Import Modules
import { BaseActor } from "./module/actor/baseActor.js";
import { ActorHnHSheet } from "./module/actor/actorHnH-sheet.js";
import { BaseItem } from "./module/item/item.js";
import { BaseItemSheet, PoolItemSheet, ModifierItemSheet, VariableItemSheet } from "./module/item/item-sheet.js";
import { config } from "./module/config.js";
import { HnHTokenDocument, HnHToken, TokenEffects } from "./module/token.js";
import { HnHCombat, HnHCombatTracker, HnHCombatant, HnHCombatantConfig } from "./module/combat/HnHCombat.js";

// Pre-load templates
async function preloadHandlebarsTemplates() {
  const templatePaths = [
    "systems/hnh/templates/partials/checks.hbs",
    "systems/hnh/templates/partials/defences-saves.hbs",
    "systems/hnh/templates/partials/gmod.hbs",
    "systems/hnh/templates/partials/attacks.hbs",
    "systems/hnh/templates/partials/modifiers.hbs",
    "systems/hnh/templates/partials/variables.hbs",
    "systems/hnh/templates/partials/notes.hbs",
    "systems/hnh/templates/partials/owneditems.hbs",
    "systems/hnh/templates/partials/owneditemstab.hbs",
    "systems/hnh/templates/partials/pools.hbs",
    "systems/hnh/templates/partials/attributes.hbs",
    "systems/hnh/templates/partials/reaction-ac.hbs",
    "systems/hnh/templates/partials/scripts.hbs",
    "systems/hnh/templates/partials/sectiontitles.hbs",
    "systems/hnh/templates/partials/skills.hbs",
    "systems/hnh/templates/partials/spells.hbs",
    "systems/hnh/templates/partials/traits.hbs"
  ];
  return loadTemplates(templatePaths);
};

Roll.CHAT_TEMPLATE = "systems/hnh/templates/dice/roll.hbs";
Roll.TOOLTIP_TEMPLATE = "systems/hnh/templates/dice/tooltip.hbs";

Hooks.once('init', () => {

  CONFIG.system = config;

  // Define custom Entity classes
  CONFIG.Actor.documentClass = BaseActor;
  CONFIG.Item.documentClass = BaseItem;
  CONFIG.Token.documentClass = HnHTokenDocument;
  CONFIG.Token.objectClass = HnHToken;

  // Which actor in this browser most recently made a roll
  game.settings.register("hnh", "currentActor", {
    name: "Current Actor Name",
    hint: "Which actor most recently made a roll on this browser?",
    scope: "client",
    config: false,
    default: "null",
    type: String
  });

  game.settings.register("hnh", "narrateJustifyLeft", {
    name: "Narrative Tools Left-Justify",
    hint: "Narrative Tools chat messages will be left-justified instead of centred.",
    scope: "client",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.system.narrateJustifyLeft = game.settings.get("hnh", "narrateJustifyLeft");

  game.settings.register("hnh", "systemMigrationVersion", {
    name: "System Migration Version",
    scope: "world",
    config: true,
    type: Number,
    default: 0.30,
    requiresReload: true
  });

  // Register Defence Variables
  game.settings.register("hnh", "defenceVariables", {
    name: "SETTINGS.DefenceVariables",
    hint: "SETTINGS.DefenceVariablesHint",
    scope: "world",
    config: true,
    restricted: true,
    default: CONFIG.system.variables.defencevariables.toString(),
    requiresReload: true,
    type: String
  });
  CONFIG.system.variables.defencevariables = game.settings.get("hnh", "defenceVariables").split(",").map(word => word.trim());

  // Register Attack Variables
  game.settings.register("hnh", "attackVariables", {
    name: "SETTINGS.AttackVariables",
    hint: "SETTINGS.AttackVariablesHint",
    scope: "world",
    config: true,
    restricted: true,
    default: CONFIG.system.variables.attackvariables.toString(),
    requiresReload: true,
    type: String
  });
  CONFIG.system.variables.attackvariables = game.settings.get("hnh", "attackVariables").split(",").map(word => word.trim());

  // Register Skill Variables
  game.settings.register("hnh", "skillVariables", {
    name: "SETTINGS.SkillVariables",
    hint: "SETTINGS.SkillVariablesHint",
    scope: "world",
    config: true,
    restricted: true,
    default: CONFIG.system.variables.skillvariables.toString(),
    requiresReload: true,
    type: String
  });
  CONFIG.system.variables.skillvariables = game.settings.get("hnh", "skillVariables").split(",").map(word => word.trim());

  // Register Spell Variables
  game.settings.register("hnh", "spellVariables", {
    name: "SETTINGS.SpellVariables",
    hint: "SETTINGS.SpellVariablesHint",
    scope: "world",
    config: true,
    restricted: true,
    default: CONFIG.system.variables.spellvariables.toString(),
    requiresReload: true,
    type: String
  });
  CONFIG.system.variables.spellvariables = game.settings.get("hnh", "spellVariables").split(",").map(word => word.trim());

  // Register Show Test Data
  game.settings.register("hnh", "showTestData", {
    name: "SETTINGS.ShowTestData",
    hint: "SETTINGS.ShowTestDataHint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.system.testMode = game.settings.get("hnh", "showTestData");

  // Register Show Hooks Data
  game.settings.register("hnh", "showHooks", {
    name: "SETTINGS.ShowHooks",
    hint: "SETTINGS.ShowHooksHint",
    scope: "world",
    config: true,
    default: false,
    requiresReload: true,
    type: Boolean
  });
  CONFIG.debug.hooks = game.settings.get("hnh", "showHooks");

  /*
  game.settings.register("hnh", "effectSize", {
    name: "SETTINGS.TokenEffectSize",
    hint: "SETTINGS.TokenEffectSizeHint",
    default: CONFIG.tokenEffects.effectSizeChoices.large,
    scope: "client",
    type: String,
    choices: CONFIG.tokenEffects.effectSizeChoices,
    config: true,
    onChange: s => {
      TokenEffects.patchCore();
      canvas.draw();
    }
  });
  TokenEffects.patchCore();
  */

  // ================ end of system settings

  // ================ beginning of system setup
  CONFIG.Combat.documentClass = HnHCombat;
  CONFIG.Combat.sheetClass = HnHCombatantConfig;
  CONFIG.Combat.initiative = { formula: "1d20 + @dynamic.initiative.system.moddedvalue", decimals: 0 };
  CONFIG.Combatant.documentClass = HnHCombatant;
  CONFIG.ui.combat = HnHCombatTracker;
  CONFIG.time.roundTime = 6;

  // Change the thickness of the border around Objects. Default = 4
  CONFIG.Canvas.objectBorderThickness = 9;

  // Register sheet application classes
  Actors.unregisterSheet("core", ActorSheet);
  Actors.registerSheet("hnh", ActorHnHSheet, {
    types: ["Hero"],
    makeDefault: true,
    label: "Heroes & Hardships Character"
  });
  Items.unregisterSheet("core", ItemSheet);
  Items.registerSheet("hnh", BaseItemSheet, {
    types: ["Attribute", "Characteristic", "Skill", "Melee-Attack", "Ranged-Attack", "Container", "Rollable", "Trait", "Defence", "Equipment"],
    makeDefault: true,
    label: "Items"
  });
  Items.registerSheet("hnh", PoolItemSheet, {
    types: ["Pool"],
    makeDefault: true,
    label: "Pool Item"
  });
  Items.registerSheet("hnh", ModifierItemSheet, {
    types: ["Modifier"],
    makeDefault: true,
    label: "Modifier Item"
  });
  Items.registerSheet("hnh", VariableItemSheet, {
    types: ["Variable"],
    makeDefault: true,
    label: "Variable Item"
  });

  preloadHandlebarsTemplates();

  // If you need to add Handlebars helpers, here are a few useful examples:
  Handlebars.registerHelper('concat', function () {
    var outStr = '';
    for (var arg in arguments) {
      if (typeof arguments[arg] != 'object') {
        outStr += arguments[arg];
      }
    }
    return outStr;
  });

  Handlebars.registerHelper('toLowerCase', function (str) {
    return str.toLowerCase();
  });

  Handlebars.registerHelper('contains', function (str, text) {
    return str.includes(text);
  });

  Handlebars.registerHelper('isModulus', function (value, div, rem) {
    return value % div == rem;
  });

  Handlebars.registerHelper('debug', function (text, content) {
    return console.debug(text, content);
  });

  Handlebars.registerHelper('toSentenceCase', function (str) {
    return str.replace(
      /\w\S*/g,
      function (txt) {
        return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
      }
    );
  });

  Handlebars.registerHelper('times', function (n, content) {
    let result = "";
    for (let i = 0; i < n; ++i) {
      result += content.fn(i);
    }
    return result;
  });

  /**
   * A helper to create a set of radio checkbox input elements in a named set.
   * The provided keys are the possible radio values while the provided values are human readable labels.
   *
   * @param {string} name         The radio checkbox field name
   * @param {object} choices      A mapping of radio checkbox values to human readable labels
   * @param {string} options.checked    Which key is currently checked?
   * @param {boolean} options.localize  Pass each label through string localization?
   * @return {Handlebars.SafeString}
   *
   * @example <caption>The provided input data</caption>
   * let groupName = "importantChoice";
   * let choices = {a: "Choice A", b: "Choice B"};
   * let chosen = "a";
   *
   * @example <caption>The template HTML structure</caption>
   * <div class="form-group">
   *   <label>Radio Group Label</label>
   *   <div class="form-fields">
   *     {{radioBoxes groupName choices checked=chosen localize=true}}
   *   </div>
   * </div>
   */
  Handlebars.registerHelper('radioLabels', function (name, choices, options) {
    const checked = options.hash['checked'] || null;
    const localize = options.hash['localize'] || false;
    let html = "";
    for (let [key, label] of Object.entries(choices)) {
      if (localize) label = game.i18n.localize(label);
      const isChecked = checked === key;
      html += `<input type="radio" id="${label}" name="${name}" value="${key}" ${isChecked ? "checked" : ""}><label for="${label}">${label}</label>`;
    }
    return new Handlebars.SafeString(html);
  });
});
